Kompilacja:
Do kompilacji potrzebny jest silnik Unreal Engine 4 w wersji 4.10.
Należy otworzyć plik GhostGame.uproject za pomoca edytora Unreal Engine, akceptujšc komunikat o kompilacji brakujacych pakietów.
Wtedy projekt skompiluje się automatycznie.
Rekompilacja projektu możliwa jest za pomoca przycisku "Compile", który znajduje się w górnym pasku edytora.

Uruchomienie:
Aby uruchomić projekt należy wcisnać przycisk "Play" znajdujacy się w górnym pasku edytora.
Za pomoca rozwijanego menu po prawej stronie przycisku można wybrać tryb gry:
VR preview, jeli używamy Oculus Rift, lub Standalone Game, aby zagrać na normalnym ekranie monitora.
