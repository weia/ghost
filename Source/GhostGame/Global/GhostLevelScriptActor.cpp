// Author: Grzegorz Bukowiec

#include "GhostGame.h"
#include "GhostLevelScriptActor.h"

void AGhostLevelScriptActor::BeginPlay()
{
	Super::BeginPlay();

	AGhostCharacter* Ghost = nullptr;
	for (AActor* Actor : GetWorld()->GetCurrentLevel()->Actors)
	{
		if (Actor != nullptr)
		{
			if (Cast<AGhostCharacter>(Actor) != nullptr)
			{
				Ghost = Cast<AGhostCharacter>(Actor);
			}
		}
	}

	if (UGameplayStatics::DoesSaveGameExist("save", 0))
	{
		SaveGame = Cast<USaveGameUtility>(UGameplayStatics::LoadGameFromSlot("save", 0));

		if (SaveGame->LevelName != *GetWorld()->GetMapName().Right(6))
		{
			Ghost->FadeOutLate();
			return;
		}

		AZombieCharacter* Zombie = nullptr;

		for (AActor* Actor : GetWorld()->GetCurrentLevel()->Actors)
		{
			if (Actor != nullptr)
			{
				if (Cast<AEntranceSensitiveBoundingBox>(Actor) != nullptr)
				{
					Cast<AEntranceSensitiveBoundingBox>(Actor)->DisableBBox();
				}
			}
		}


		for (AActor* Actor : GetWorld()->GetCurrentLevel()->Actors)
		{
			if (Actor != nullptr)
			{
				if (SaveGame->InteractiveStates.Contains(Actor->GetName()))
				{
					if (*(SaveGame->InteractiveStates.Find(Actor->GetName())))
					{
						Cast<IInteractive>(Actor)->SetActivation(true);
					}
					else
					{
						Cast<IInteractive>(Actor)->SetActivation(false);
					}
				}
			}
		}
		for (AActor* Actor : GetWorld()->GetCurrentLevel()->Actors)
		{
			if (Actor != nullptr)
			{
				if (SaveGame->FunctionalStates.Contains(Actor->GetName()))
				{
					if (*(SaveGame->FunctionalStates.Find(Actor->GetName())))
					{
						Cast<IFunctional>(Actor)->ActivateFunctionality(false, false);
					}
					else
					{
						Cast<IFunctional>(Actor)->DeactivateFunctionality(false, false);
					}
				}
			}
		}

		TMap<FString, bool> StaticMeshActorsPhysics;

		for (AActor* Actor : GetWorld()->GetCurrentLevel()->Actors)
		{
			if (Actor != nullptr)
			{
				if (SaveGame->ActorPositions.Contains(Actor->GetName()))
				{
					if (Cast<AStaticMeshActor>(Actor) != nullptr)
					{
						StaticMeshActorsPhysics.Add(Actor->GetName(), Cast<AStaticMeshActor>(Actor)->GetStaticMeshComponent()->IsSimulatingPhysics());
						Cast<AStaticMeshActor>(Actor)->GetStaticMeshComponent()->SetSimulatePhysics(false);
					}
					FActorData* ActorData = SaveGame->ActorPositions.Find(Actor->GetName());
					Actor->SetActorLocationAndRotation(ActorData->Location, ActorData->Rotation, false, nullptr, ETeleportType::TeleportPhysics);
				}

				if (Cast<ATriggeredInfo>(Actor) != nullptr && !SaveGame->TriggersLeft.Contains(Actor->GetName()))
				{
					Actor->Destroy();
				}

				if (Cast<AGhostCharacter>(Actor) != nullptr)
				{
					Ghost->SetActorLocation(SaveGame->PawnLocation);
				}

				if (Actor->GetName() == SaveGame->PawnName)
				{
					Zombie = Cast<AZombieCharacter>(Actor);
				}

				if (Cast<AMusicManager>(Actor) != nullptr)
				{
					Cast<AMusicManager>(Actor)->SetPlayedSoundIndex(SaveGame->SoundIndex);
				}
			}
		}

		for (AActor* Actor : GetWorld()->GetCurrentLevel()->Actors)
		{
			if (Actor != nullptr)
			{
				if (SaveGame->ActorPositions.Contains(Actor->GetName()))
				{
					if (StaticMeshActorsPhysics.Contains(Actor->GetName()))
					{
						Cast<AStaticMeshActor>(Actor)->GetStaticMeshComponent()->SetSimulatePhysics(*StaticMeshActorsPhysics.Find(Actor->GetName()));
					}
				}
			}
		}

		for (AActor* Actor : GetWorld()->GetCurrentLevel()->Actors)
		{
			if (Actor != nullptr)
			{
				if (Cast<AEntranceSensitiveBoundingBox>(Actor) != nullptr)
				{
					Cast<AEntranceSensitiveBoundingBox>(Actor)->CountAllObjects(false);
					Cast<AEntranceSensitiveBoundingBox>(Actor)->EnableBBox();
				}
			}
		}

		for (AActor* Actor : GetWorld()->GetCurrentLevel()->Actors)
		{
			if (Actor != nullptr)
			{
				if (SaveGame->CountingObjectValues.Contains(Actor->GetName()))
				{
					Cast<AFunctionalCountingObject>(Actor)->SetNumberOfActivations(*(SaveGame->CountingObjectValues.Find(Actor->GetName())));
				}
			}
		}

		if (Zombie == nullptr)
		{
			Ghost->FadeOutLate();
		}
		else {
			Zombie->Interact(Ghost);
			Ghost->InteractWith(Zombie);
		}
	}
	else
	{
		Ghost->FadeOutLate();
	}
}