// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/LevelScriptActor.h"
#include "SaveGameUtility.h"
#include "../Utilities/FunctionalObjectsArray.h"
#include "../Characters/GhostCharacter.h"
#include "../Characters/ZombieCharacter.h"
#include "../FunctionalObjects/FunctionalCountingObject.h"
#include "../EntranceSensitiveObjects/EntranceSensitiveBoundingBox.h"
#include "../Effects/TriggeredInfo.h"
#include "GhostLevelScriptActor.generated.h"

/**
 * 
 */
UCLASS()
class GHOSTGAME_API AGhostLevelScriptActor : public ALevelScriptActor
{
	GENERATED_BODY()
private:
	USaveGameUtility* SaveGame;
	UInputComponent* InputComponent;

protected:
	virtual void BeginPlay() override;
};