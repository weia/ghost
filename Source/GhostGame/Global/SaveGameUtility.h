// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/SaveGame.h"
#include "SaveGameUtility.generated.h"

/**
 * 
 */
USTRUCT()
struct FActorData {
	GENERATED_USTRUCT_BODY()
		UPROPERTY(EditAnywhere)
		FVector Location;
	UPROPERTY(EditAnywhere)
		FRotator Rotation;
	
	FActorData() {}
	FActorData(FVector NewLocation, FRotator NewRotation)
	{
		Location = NewLocation;
		Rotation = NewRotation;
	}
};

UCLASS()
class GHOSTGAME_API USaveGameUtility : public USaveGame
{
	GENERATED_BODY()

public:
	UPROPERTY(VisibleAnywhere, Category = Basic)
		FString LevelName;
	UPROPERTY(VisibleAnywhere, Category = Basic)
		FString PawnName;
	UPROPERTY(VisibleAnywhere, Category = Basic)
		FVector PawnLocation;
	UPROPERTY(VisibleAnywhere, Category = Basic)
		int32 SoundIndex;
	UPROPERTY(VisibleAnywhere, Category = Basic)
		TMap<FString, FActorData> ActorPositions;
	UPROPERTY(VisibleAnywhere, Category = Basic)
		TMap<FString, uint8> FunctionalStates;
	UPROPERTY(VisibleAnywhere, Category = Basic)
		TMap<FString, uint8> InteractiveStates;
	UPROPERTY(VisibleAnywhere, Category = Basic)
		TMap<FString, int32> CountingObjectValues;
	UPROPERTY(VisibleAnywhere, Category = Basic)
		TArray<FString> TriggersLeft;

	USaveGameUtility();
};