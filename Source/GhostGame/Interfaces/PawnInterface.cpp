// Fill out your copyright notice in the Description page of Project Settings.

#include "GhostGame.h"
#include "PawnInterface.h"

UPawnInterface::UPawnInterface(const class FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}

void IPawnInterface::DisplayInfo(FString TextInfo)
{
}

void IPawnInterface::DisplayObjectInfo(IDisplayable* TracedActor, FInteractCapability* InteractCapability)
{
}

void IPawnInterface::HideObjectInfo()
{
}

bool IPawnInterface::IsInteractWithEnabled(AActor* actor, FInteractCapability* output)
{
	return true;
}

void IPawnInterface::InteractWith(AActor* actor)
{
}

FString IPawnInterface::GetInteractiveName()
{
	return "Unknown";
}

int IPawnInterface::GetPawnAttributeValue(FString AttributeName)
{
	return 0;
}