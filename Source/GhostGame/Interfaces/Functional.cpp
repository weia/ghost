// Fill out your copyright notice in the Description page of Project Settings.

#include "GhostGame.h"
#include "Functional.h"

UFunctional::UFunctional(const class FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}

void IFunctional::ActivateFunctionality(bool bIsAnimationSynchronized, bool bShowAnimation)
{
}

void IFunctional::DeactivateFunctionality(bool bIsAnimationSynchronized, bool bShowAnimation)
{
}

void IFunctional::BeginOfAnimation(IInteractive* AActor)
{
}

void IFunctional::EndOfAnimation()
{
}

void IFunctional::AnimationTimelineFloatReturn(float val)
{
}

bool IFunctional::CanActivate()
{
	return true;
}

bool IFunctional::CanDeactivate()
{
	return true;
}

void IFunctional::ScheduleActivation()
{
	ActivateFunctionality(false);
}

void IFunctional::ScheduleDeactivation()
{
	DeactivateFunctionality(false);
}

bool IFunctional::IsFunctionalityActive()
{
	return false;
}