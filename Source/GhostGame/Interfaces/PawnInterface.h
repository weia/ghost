// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "../Components/PlayerInfoComponent.h"
#include "../Interfaces/Displayable.h"
#include "../Utilities/InteractCapability.h"
#include "PawnInterface.generated.h"

UINTERFACE(MinimalAPI)
class UPawnInterface : public UInterface 
{
	GENERATED_UINTERFACE_BODY()
};

class IPawnInterface
{
	GENERATED_IINTERFACE_BODY()

	virtual void DisplayInfo(FString TextInfo);
	virtual void DisplayObjectInfo(IDisplayable* TracedActor, FInteractCapability* InteractCapability);
	virtual void HideObjectInfo();

	// Functions for tracing component

	// Specify if interaction should be performed when player presses button
	// (You may want to disable interaction if our pawn can't interact with objects at the time)
	virtual bool IsInteractWithEnabled(AActor* actor, FInteractCapability* output);

	// Called when interaction is performed 
	virtual void InteractWith(AActor* actor);

	// Funtions for interacting

	virtual FString GetInteractiveName();

	virtual int GetPawnAttributeValue(FString AttrubuteName);
};