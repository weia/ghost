// Fill out your copyright notice in the Description page of Project Settings.

#include "GhostGame.h"
#include "Interactive.h"



UInteractive::UInteractive(const class FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}

void IInteractive::Interact(AActor *actor)
{
}

bool IInteractive::IsInteractEnabled(AActor *actor, FInteractCapability* output)
{
	return true;
}

void IInteractive::PauseSynchronizedAnimation()
{
}

void IInteractive::ResumeSynchronizedAnimation()
{
}

bool IInteractive::IsActivated()
{
	return false;
}

void IInteractive::SetActivation(bool bIsActivated)
{
}