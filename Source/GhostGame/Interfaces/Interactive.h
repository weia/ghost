// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "../Utilities/InteractCapability.h"
#include "Interactive.generated.h"

UINTERFACE(MinimalAPI)
	class UInteractive : public UInterface
{
	GENERATED_UINTERFACE_BODY()	
};


class IInteractive
{
	GENERATED_IINTERFACE_BODY()
	//Called when interaction is performed
	virtual void Interact(AActor* actor);

	// Specify if interaction should be performed when player presses button
	// (You may want to disable interaction if object can't interact at the time)
	virtual bool IsInteractEnabled(AActor *actor, FInteractCapability* output);

	virtual void PauseSynchronizedAnimation();
	virtual void ResumeSynchronizedAnimation();

	virtual bool IsActivated();
	virtual void SetActivation(bool bIsActivated);
};