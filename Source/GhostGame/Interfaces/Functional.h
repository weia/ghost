// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "../Interfaces/Interactive.h"
#include "Functional.generated.h"

UINTERFACE(MinimalAPI)
class UFunctional : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};


class IFunctional
{
	GENERATED_IINTERFACE_BODY()

	virtual void ActivateFunctionality(bool bIsAnimationSynchronized, bool bShowAnimation = true);
	virtual void DeactivateFunctionality(bool bIsAnimationSynchronized, bool bShowAnimation = true);
	virtual void BeginOfAnimation(IInteractive* AActor = nullptr);
	virtual void EndOfAnimation();
	virtual void AnimationTimelineFloatReturn(float val);
	virtual bool CanActivate();
	virtual bool CanDeactivate();
	virtual void ScheduleActivation();
	virtual void ScheduleDeactivation();
	virtual bool IsFunctionalityActive();
};