// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "../Utilities/PawnAttribute.h"
#include "Displayable.generated.h"

UINTERFACE(MinimalAPI)
class UDisplayable : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};


class IDisplayable
{
	GENERATED_IINTERFACE_BODY()

	// Get name that should be showed to player as object's name
	virtual FString GetNameToDisplay();

	// Get the central location of an actor
	virtual FVector GetDisplayLocation();

	// Get attributes of an actor
	virtual TArray<FPawnAttribute> GetAttributes();
};