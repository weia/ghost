// Fill out your copyright notice in the Description page of Project Settings.

#include "GhostGame.h"
#include "Displayable.h"

UDisplayable::UDisplayable(const class FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}


FString IDisplayable::GetNameToDisplay()
{
	return "Unidentified object";
}

FVector IDisplayable::GetDisplayLocation()
{
	FVector Origin, TracedActorBounds;
	Cast<AActor>(this)->GetActorBounds(true, Origin, TracedActorBounds);

	return Cast<AActor>(this)->GetActorLocation() + FVector(0.0f, 0.0f, TracedActorBounds.Z + 25.0f);
}

TArray<FPawnAttribute> IDisplayable::GetAttributes()
{
	return TArray<FPawnAttribute>();
}