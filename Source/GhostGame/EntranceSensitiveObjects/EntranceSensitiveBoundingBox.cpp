// Author: Anna Kobak

#include "GhostGame.h"
#include "EntranceSensitiveBoundingBox.h"


// Sets default values
AEntranceSensitiveBoundingBox::AEntranceSensitiveBoundingBox()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SetActorEnableCollision(true);
	FunctionalObjectsStructure.SetArray(&FunctionalObjects);

	BoundingBox = CreateDefaultSubobject<UBoxComponent>(TEXT("Trigger"));
	RootComponent = BoundingBox;
	BoundingBox->InitBoxExtent(FVector(50.0f, 50.0f, 50.0f));
	BoundingBox->SetCollisionResponseToChannel(ECollisionChannel::ECC_WorldStatic, ECollisionResponse::ECR_Ignore);
	BoundingBox->SetCollisionResponseToChannel(ECollisionChannel::ECC_Visibility, ECollisionResponse::ECR_Ignore);
	BoundingBox->SetCollisionResponseToChannel(ECollisionChannel::ECC_Camera, ECollisionResponse::ECR_Ignore);
	BoundingBox->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Ignore);
	BoundingBox->SetCollisionResponseToChannel(ECollisionChannel::ECC_GameTraceChannel1, ECollisionResponse::ECR_Ignore);
	BoundingBox->SetCollisionResponseToChannel(ECollisionChannel::ECC_GameTraceChannel2, ECollisionResponse::ECR_Ignore);

}

// Called when the game starts or when spawned
void AEntranceSensitiveBoundingBox::BeginPlay()
{
	Super::BeginPlay();
	OnActorBeginOverlap.AddDynamic(this, &AEntranceSensitiveBoundingBox::BoundingBoxBeginOverlap);
	OnActorEndOverlap.AddDynamic(this, &AEntranceSensitiveBoundingBox::BoundingBoxEndOverlap);	
	if (bIsEnabled) {
		CountAllObjects(true);
		FunctionalObjectsStructure.PerformBeginPlayActivation(bIsActivated);
	}
}

void AEntranceSensitiveBoundingBox::PostLoad()
{
	Super::PostLoad();
	if(bIsEnabled)
		CountAllObjects(true);
}

void AEntranceSensitiveBoundingBox::CountAllObjects(bool bPerformActivations)
{
	NumberOfObjects = 0;
	MassOfObjects = 0;

	TArray<AActor*> Actors;
	BoundingBox->GetOverlappingActors(Actors);
	for (AActor* Actor : Actors)
		if (IsActorRelevant(Actor)) {
			NumberOfObjects++;
			MassOfObjects += GetActorMass(Actor);
		}

	UpdateConditions(bPerformActivations);
	if (bConditionsSatisfied)
		bIsActivated = true;
	else
		bIsActivated = false;
}


// Called every frame
void AEntranceSensitiveBoundingBox::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	TimePassed += DeltaTime;
	if(bIsEnabled)
		UpdateActivation();
}

void AEntranceSensitiveBoundingBox::BoundingBoxBeginOverlap(AActor* Other)
{
	if (bIsEnabled) {
		if (!IsActorRelevant(Other))
			return;

		NumberOfObjects++;
		MassOfObjects += GetActorMass(Other);
		UpdateConditions();
	}
}

void AEntranceSensitiveBoundingBox::BoundingBoxEndOverlap(AActor* Other)
{
	if (bIsEnabled) {
		if (!IsActorRelevant(Other))
			return;

		NumberOfObjects--;
		MassOfObjects -= GetActorMass(Other);
		UpdateConditions();
	}
}



float AEntranceSensitiveBoundingBox::GetActorMass(AActor * Actor)
{
	float Mass = 0.0f;
	TArray<UActorComponent*> Components = Actor->GetComponents();
	for (UActorComponent* Component : Components) {
		if (Component == nullptr)
			return 0.0f;
		UPrimitiveComponent* PrimitiveComponent = Cast<UPrimitiveComponent>(Component);
		if (PrimitiveComponent != nullptr) {
			Mass += PrimitiveComponent->CalculateMass();
		}
	}
	return Mass;
}

bool AEntranceSensitiveBoundingBox::IsActorRelevant(AActor* Actor) {
	return (bReactToAllActors && !IgnoreActors.Contains(Actor)) || (!bReactToAllActors && NeverIgnoreActors.Contains(Actor));
}



void AEntranceSensitiveBoundingBox::UpdateConditions(bool bPerformActivations)
{
	bool bMassCondition = (MassOfObjects >= MinimalMassOfObjects) || !bReactToMassOfObjects;
	bool bNumberCondition = (NumberOfObjects >= MinimalNumberOfObjects) || !bReactToNumberOfObjects;
	bool bAllConditions = bMassCondition && bNumberCondition;

	if (bAllConditions ^ bConditionsSatisfied)
		TimePassed = 0.0f;

	bConditionsSatisfied = bAllConditions;

	UpdateActivation(bPerformActivations);
}

void AEntranceSensitiveBoundingBox::UpdateActivation(bool bPerformActivations)
{
	bool bTimeCondition = TimePassed >= (bIsActivated ? DereactionTime : ReactionTime);
	if (bPerformActivations && !bTimeCondition)
		return;
	if (bConditionsSatisfied && !bIsActivated) {
		if (bPerformActivations) 
			FunctionalObjectsStructure.CallScheduleActivationInAll();
		bIsActivated = true;
	}
	if (!bConditionsSatisfied && bIsActivated) {
		if (bPerformActivations)
			FunctionalObjectsStructure.CallScheduleDeactivationInAll();
		bIsActivated = false;
	}
}

void AEntranceSensitiveBoundingBox::EnableBBox()
{
	bIsEnabled = true;
}

void AEntranceSensitiveBoundingBox::DisableBBox()
{
	bIsEnabled = false;
}