// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "../Utilities/FunctionalObjectsArray.h"
#include "EntranceSensitiveBoundingBox.generated.h"

UCLASS()
class GHOSTGAME_API AEntranceSensitiveBoundingBox : public AActor
{
	GENERATED_BODY()
	//Variables
	int NumberOfObjects = 0;
	float MassOfObjects = 0;
	float TimePassed = 0;
	bool bIsActivated = false;
	bool bConditionsSatisfied = false;

	bool bIsEnabled = true;

	FFunctionalObjectsArray FunctionalObjectsStructure;

	//Functions
	float GetActorMass(AActor * Actor);
	void UpdateConditions(bool bPerformActivations = true);
	void UpdateActivation(bool bPerformActivations = true);

	//Returns true if actor is not ignored
	bool IsActorRelevant(AActor* Actor);

public:
	//Properties

	// Enable/Disable Number Condition
	// Whether to pay attention to number of objects in our bounding box or not
	UPROPERTY(EditAnywhere)
		bool bReactToNumberOfObjects = false;
	// Enable/Disable Mass Condition
	// Whether to pay attention to mass of objects in our bounding box or not
	UPROPERTY(EditAnywhere)
		bool bReactToMassOfObjects = false;
	// How many objects should be in our bounding box to satisfy Number Condition
	UPROPERTY(EditAnywhere)
		int32 MinimalNumberOfObjects = 1;
	// How much mass should be in our bounding box to satisfy Mass Condition
	UPROPERTY(EditAnywhere)
		float MinimalMassOfObjects;

	// If both mass Condition and Number Condition are satisfied, the bounding box will call
	// Activate Functionallity in all FunctionalObjects, after the time specified in ReactionTime passes
	// If one of the condition ceases to be fulfilled, 
	// DeactivateFunctionality in all FunctionalObjects is called, after ReactionTime passes.

	// Time that should pass between satisfaction of all conditions 
	// and activatation of functional components
	UPROPERTY(EditAnywhere)
		float ReactionTime = 0.0f;
	// Time that should pass between satisfaction of all conditions 
	// and deactivatation of functional components
	UPROPERTY(EditAnywhere)
		float DereactionTime = 0.0f;
	//Defines whether component will consider all objects as overlapping
	//If set to true, only objects from IgnoreActors will be ignored
	//If set to false, only objects from NeverIgnoreActors will not be ignored
	UPROPERTY(EditAnywhere)
		bool bReactToAllActors = true;
	UPROPERTY(EditAnywhere)
		TArray<AActor*> IgnoreActors;
	UPROPERTY(EditAnywhere)
		TArray<AActor*> NeverIgnoreActors;
	//List of objects which should be called, when something begins to overlap with our object
	//ActivateFunctionality is called when first objects enters our box,
	//DeactivateFunctionality is called, when our box is empty
	//Called on BeginPlay too
	UPROPERTY(EditAnywhere)
		TArray<FFunctionalPair> FunctionalObjects;

	//Components
	UPROPERTY(EditAnywhere)
		UBoxComponent* BoundingBox;

	//Functiones
	AEntranceSensitiveBoundingBox();
	virtual void BeginPlay() override;
	virtual void PostLoad() override;
	void CountAllObjects(bool bPerformActivations);
	void DisableBBox();
	void EnableBBox();

	virtual void Tick( float DeltaSeconds ) override;

	UFUNCTION()
		void BoundingBoxBeginOverlap(AActor* Other);
	UFUNCTION()
		void BoundingBoxEndOverlap(AActor* Other);
};
