// Fill out your copyright notice in the Description page of Project Settings.

#include "GhostGame.h"
#include "MenuGameMode.h"
#include "Characters/MenuCharacter.h"

AMenuGameMode::AMenuGameMode(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	DefaultPawnClass = AMenuCharacter::StaticClass();
}


