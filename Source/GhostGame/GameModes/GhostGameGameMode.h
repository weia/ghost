// Defines Ghost as a default pawn

#pragma once

#include "GameFramework/GameMode.h"
#include "GhostGameGameMode.generated.h"

UCLASS()
class GHOSTGAME_API AGhostGameGameMode : public AGameMode
{
	GENERATED_BODY()
	
	AGhostGameGameMode(const FObjectInitializer& ObjectInitializer);
};
