// Defines Ghost as a default pawn

#include "GhostGame.h"
#include "GhostGameGameMode.h"
#include "Characters/GhostCharacter.h"

AGhostGameGameMode::AGhostGameGameMode(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	DefaultPawnClass = AGhostCharacter::StaticClass();
}



