// Author: Anna Kobak

#include "GhostGame.h"
#include "InteractiveLock.h"


// Sets default values
AInteractiveLock::AInteractiveLock()
{
	PrimaryActorTick.bCanEverTick = true;

	//Frame Mesh
	FrameMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("FrameMeshComponent"));
	static ConstructorHelpers::FObjectFinder<UStaticMesh> FrameStaticMesh(TEXT("StaticMesh'/Game/Meshes/Objects/Object_Lock.Object_Lock'"));
	static ConstructorHelpers::FObjectFinder<UMaterial> FrameMaterial1(TEXT("Material'/Game/StarterContent/Materials/M_Concrete_Tiles.M_Concrete_Tiles'"));
	static ConstructorHelpers::FObjectFinder<UMaterial> FrameMaterial2(TEXT("Material'/Game/StarterContent/Materials/M_Basic_WallBlack.M_Basic_WallBlack'"));
	FrameMeshComponent->SetStaticMesh(FrameStaticMesh.Object);
	FrameMeshComponent->SetMaterial(0, FrameMaterial1.Object);
	FrameMeshComponent->SetMaterial(1, FrameMaterial2.Object);
	FrameMeshComponent->SetRelativeScale3D(FVector(0.4, 0.4, 0.4));
	FrameMeshComponent->AttachTo(RootComponent);

	//Others
	DisplayableName = "Lock";
	RequiredSkills.Add(FPawnAttribute("Security", 20));

	SoundPath = TEXT("SoundWave'/Game/Audio/Door_Unlock-SoundBible_com-1558114225.Door_Unlock-SoundBible_com-1558114225'");
	Super::CreateSound();
}

// Called when the game starts or when spawned
void AInteractiveLock::BeginPlay()
{
	Super::BeginPlay();
}


bool AInteractiveLock::IsInteractEnabled(AActor* Actor, FInteractCapability* output)
{
	if (isActivated)
		output->CanObjectInteract = false;

	if (isActivated || Super::IsInteractEnabled(Actor, output) == false)
		return false;
	return true;
}

void AInteractiveLock::Interact(AActor* actor)
{
	isActivated = true;
	FunctionalObjectsStructure.CallActivateFunctionalityInAll();
	if(SoundComponent != nullptr && bIsSoundEnabled)
		SoundComponent->Play();

}