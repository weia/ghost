// Author: Grzegorz Bukowiec

#include "GhostGame.h"
#include "MenuOptionPlay.h"

void AMenuOptionPlay::Interact(AActor* actor)
{
	Cast<AGhostCharacter>(actor)->FadeIn();
	FTimerHandle UnusedHandle;
	GetWorld()->GetTimerManager().SetTimer(UnusedHandle, this, &AMenuOptionPlay::EnterLevel, 3.5f, false);
}

void AMenuOptionPlay::EnterLevel()
{
	if (bLoadGame && UGameplayStatics::DoesSaveGameExist("save", 0))
	{
		USaveGameUtility* SaveGame = Cast<USaveGameUtility>(UGameplayStatics::LoadGameFromSlot("save", 0));
		UGameplayStatics::SaveGameToSlot(SaveGame, "save", 0);
		UGameplayStatics::OpenLevel(GetWorld(), FName(*SaveGame->LevelName));
	}
	else
	{
		if (UGameplayStatics::DoesSaveGameExist("save", 0))
		{
			UGameplayStatics::DeleteGameInSlot("save", 0);
		}
		UGameplayStatics::OpenLevel(GetWorld(), "Level1");
	}
}
