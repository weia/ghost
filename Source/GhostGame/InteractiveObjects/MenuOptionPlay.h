// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "InteractiveObjects/MenuOption.h"
#include "../Global/SaveGameUtility.h"
#include "../Characters/GhostCharacter.h"
#include "MenuOptionPlay.generated.h"

/**
 * 
 */
UCLASS()
class GHOSTGAME_API AMenuOptionPlay : public AMenuOption
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere)
		bool bLoadGame = false;

	virtual void Interact(AActor* actor) override;
	void EnterLevel();
};
