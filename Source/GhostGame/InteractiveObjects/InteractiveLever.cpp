// Author: Anna Kobak

#include "GhostGame.h"
#include "InteractiveLever.h"


// Sets default values
AInteractiveLever::AInteractiveLever()
{
	PrimaryActorTick.bCanEverTick = false;

	//Frame Mesh1
	FrameMeshComponent1 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("FrameMeshComponent1"));
	static ConstructorHelpers::FObjectFinder<UStaticMesh> FrameStaticMesh1(TEXT("StaticMesh'/Game/StarterContent/Shapes/Shape_Trim.Shape_Trim'"));
	static ConstructorHelpers::FObjectFinder<UMaterial> FrameMaterial(TEXT("Material'/Game/StarterContent/Materials/M_Concrete_Panels.M_Concrete_Panels'"));
	FrameMeshComponent1->SetStaticMesh(FrameStaticMesh1.Object);
	FrameMeshComponent1->SetMaterial(0, FrameMaterial.Object);
	FrameMeshComponent1->SetWorldLocation(FVector(10.0f, 0.0f, -10.0f));
	FrameMeshComponent1->AttachTo(RootComponent);

	//Frame Mesh2
	FrameMeshComponent2 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("FrameMeshComponent2"));
	static ConstructorHelpers::FObjectFinder<UStaticMesh> FrameStaticMesh2(TEXT("StaticMesh'/Game/StarterContent/Shapes/Shape_Trim.Shape_Trim'"));
	FrameMeshComponent2->SetStaticMesh(FrameStaticMesh2.Object);
	FrameMeshComponent2->SetMaterial(0, FrameMaterial.Object);
	FrameMeshComponent2->SetWorldLocation(FVector(-10.0f, 0.0f, -10.0f));
	FrameMeshComponent2->SetWorldRotation(FRotator(0.0f, 180.0f, 0.0f));
	FrameMeshComponent2->AttachTo(RootComponent);

	//Lever Mesh
	LeverMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("LeverMeshComponent"));
	static ConstructorHelpers::FObjectFinder<UStaticMesh> LeverStaticMesh(TEXT("StaticMesh'/Game/StarterContent/Shapes/Shape_Pipe.Shape_Pipe'"));
	static ConstructorHelpers::FObjectFinder<UMaterial> LeverMaterial(TEXT("Material'/Game/StarterContent/Materials/M_Concrete_Grime.M_Concrete_Grime'"));
	LeverMeshComponent->SetStaticMesh(LeverStaticMesh.Object);
	LeverMeshComponent->SetMaterial(0, LeverMaterial.Object);
	LeverMeshComponent->SetWorldScale3D(FVector(0.5f, 0.6f, 0.5f));
	LeverMeshComponent->SetWorldRotation(FRotator(0.0f, 0.0f, 50.0f));
	LeverMeshComponent->SetWorldLocation(FVector(0.0f, -30.0f, 10.0f));
	LeverMeshComponent->AttachTo(RootComponent);

	//Other
	DisplayableName = "Lever";
	TimelinePath = TEXT("CurveFloat'/Game/Blueprints/Timelines/LeverAnimationCurveBP.LeverAnimationCurveBP'");
	CreateTimeline();

	SoundPath = TEXT("SoundWave'/Game/Audio/167048__drminky__old-lever-pull.167048__drminky__old-lever-pull'");
	CreateSound();

	OriginalRotation = LeverMeshComponent->RelativeRotation;
	OriginalLocation = LeverMeshComponent->RelativeLocation;
}

// Called when the game starts or when spawned
void AInteractiveLever::BeginPlay()
{
	Super::BeginPlay();
}


void AInteractiveLever::AnimationTimelineFloatReturn(float val)
{
	Super::AnimationTimelineFloatReturn(val);

	FVector RelativeFinalLocation = FVector(0.0f, 40.0f, 0.0f);
	if (!(isActivated ^ isActiveAtStart)) {
		LeverMeshComponent->SetRelativeRotation(OriginalRotation + FRotator(0.0f, 0.0f, 80.0f*(1.0f - val)));
		LeverMeshComponent->SetRelativeLocation(OriginalLocation + RelativeFinalLocation*(1.0f-val));
	} 	else {
		LeverMeshComponent->SetRelativeRotation(OriginalRotation + FRotator(0.0f, 0.0f, 80.0f*val));
		LeverMeshComponent->SetRelativeLocation(OriginalLocation + RelativeFinalLocation*val);
	}
}
