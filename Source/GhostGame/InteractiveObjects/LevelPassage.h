// Changes level on override with pawn

#pragma once

#include "GameFramework/Actor.h"
#include "../Interfaces/Displayable.h"
#include "../Interfaces/Interactive.h"
#include "../Characters/GhostCharacter.h"
#include "LevelPassage.generated.h"

UCLASS()
class GHOSTGAME_API ALevelPassage : public AActor, public IDisplayable, public IInteractive
{
	GENERATED_BODY()

public:	
	// Sets default values for this actor's properties
	ALevelPassage();
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	// Called every frame
	virtual void Tick(float DeltaSeconds) override;

	// Interactive functions
	virtual void Interact(AActor* actor) override;
	virtual bool IsInteractEnabled(AActor* actor, FInteractCapability* output) override;

	// Displayable functions
	virtual FString GetNameToDisplay() override;
	//virtual FVector GetDisplayLocation() override;

	// New level's name
	UPROPERTY(EditAnywhere) 
		FName NewLevelName;
	UPROPERTY(EditAnywhere)
		FString DisplayableName;
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* Door;

	void EnterLevel();
};
