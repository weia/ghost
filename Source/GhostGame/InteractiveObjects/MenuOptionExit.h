// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "InteractiveObjects/MenuOption.h"
#include "MenuOptionExit.generated.h"

/**
 * 
 */
UCLASS()
class GHOSTGAME_API AMenuOptionExit : public AMenuOption
{
	GENERATED_BODY()
	
public:
	virtual void Interact(AActor* actor) override;
};
