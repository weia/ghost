// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "../Interfaces/Interactive.h"
#include "../Interfaces/Displayable.h"
#include "MenuOption.generated.h"

/**
 * 
 */
UCLASS()
class GHOSTGAME_API AMenuOption : public AActor, public IInteractive, public IDisplayable
{
	GENERATED_BODY()

private:
	UPROPERTY(EditAnywhere)
		UTextRenderComponent* TextRenderComponent;
	UPROPERTY(EditAnywhere)
		UBoxComponent* BoxComponent;
	UMaterialInterface* TextMaterial;

public:
	AMenuOption();

	// Called when the game starts
	virtual void BeginPlay() override;
	
	virtual FString GetNameToDisplay() override;
	virtual void Interact(AActor* actor) override;

	UPROPERTY(EditAnywhere)
		FString CommandName;
};
