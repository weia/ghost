// Author: Anna Kobak

#include "GhostGame.h"
#include "InteractiveBaseObject.h"


// Sets default values
AInteractiveBaseObject::AInteractiveBaseObject()
{
	PrimaryActorTick.bCanEverTick = false;
	FunctionalObjectsStructure.SetArray(&FunctionalObjects);

	//Root Component
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));

	InteractWithCharacters.Add("Zombie");
}

//Call from constructor only!!!
void AInteractiveBaseObject::CreateTimeline()
{
	ConstructorHelpers::FObjectFinder<UCurveFloat> Curve(TimelinePath);
	AnimationCurve = Curve.Object;
	AnimationTimeline = CreateDefaultSubobject<UTimelineComponent>(TEXT("AnimationTimeline"));
	AnimationTimeline->SetTimelineLength(1.0f);
	InterpFunction.BindUFunction(this, FName{ TEXT("AnimationTimelineFloatReturn") });
	TimelineFinishedEvent.BindUFunction(this, FName{ TEXT("TimelineFinishedFunction") });

	bIsTimelineCreated = true;
}

void AInteractiveBaseObject::CreateSound()
{
	SoundComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("SoundComponent"));
	ConstructorHelpers::FObjectFinder<USoundBase> SoundHelper(SoundPath);
	SoundComponent->bAutoActivate = false;
	SoundComponent->SetSound(SoundHelper.Object);
	SoundComponent->AttachTo(RootComponent);
	SoundComponent->SetRelativeLocation(FVector(0, 0, 0));

}

// Called when the game starts or when spawned
void AInteractiveBaseObject::BeginPlay()
{
	Super::BeginPlay();

	if (bIsTimelineCreated) {
		AnimationTimeline->SetTimelineFinishedFunc(TimelineFinishedEvent);
		AnimationTimeline->AddInterpFloat(AnimationCurve, InterpFunction, FName{ TEXT("InterpFloat") });
		AnimationTimeline->SetPlayRate(TimeToCompleteAction == 0 ? 100.0f : (1.0f / TimeToCompleteAction));
	}
	if (!bIsManuallySet) {
		isActivated = isActiveAtStart;
		FunctionalObjectsStructure.PerformBeginPlayActivation(isActivated);
	}
}

// Called every frame
void AInteractiveBaseObject::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
}

FString AInteractiveBaseObject::GetNameToDisplay()
{
	return DisplayableName;
}

void AInteractiveBaseObject::ActivateFunctionality(bool bIsAnimationSynchronized, bool bShowAnimation)
{
	isActive = false;
}

void AInteractiveBaseObject::DeactivateFunctionality(bool bIsAnimationSynchronized, bool bShowAnimation)
{
	isActive = true;
}

void AInteractiveBaseObject::AnimationTimelineFloatReturn(float val)
{
	FunctionalObjectsStructure.CallAnimationTimelineFloatReturnInAll(val);
}

void AInteractiveBaseObject::TimelineFinishedFunction()
{
	bIsAnimationRunning = false;
	FunctionalObjectsStructure.CallEndOfAnimationInAll();
	if (bStopSoundAtAnimationFinished && SoundComponent != nullptr)
		SoundComponent->Stop();
}

bool AInteractiveBaseObject::IsInteractEnabled(AActor* actor, FInteractCapability* output)
{
	bool bIsInteractEnabled = true;
	if (!isActive) {
		output->CanObjectInteract = false;
		bIsInteractEnabled =  false;
	}
	IPawnInterface* Pawn = Cast<IPawnInterface>(actor);
	if (!InteractWithCharacters.Contains(Pawn->GetInteractiveName())) {
		output->CanPawnInteract = false;
		bIsInteractEnabled = false;
	}
	for (int i = 0; i < RequiredSkills.Num(); i++) {
		if (Pawn->GetPawnAttributeValue(RequiredSkills[i].Name) < RequiredSkills[i].Value) {
			output->SkillsMissing.Add(RequiredSkills[i].Name);
			bIsInteractEnabled = false;
		}
	}
	return bIsInteractEnabled;

}

void AInteractiveBaseObject::Interact(AActor* actor)
{
	if (bIsTimelineCreated && bIsAnimationRunning)
		return;
	if (!isDeactivateEnabled || !isActivated)
		FunctionalObjectsStructure.CallActivateFunctionalityInAll(this);
	else
		FunctionalObjectsStructure.CallDeactivateFunctionalityInAll(this);
	if (SoundComponent != nullptr && bIsSoundEnabled)
		SoundComponent->Play();
	if (bIsTimelineCreated) {
		bIsAnimationRunning = true;
		AnimationTimeline->PlayFromStart();
	}
	isActivated = !isActivated;
}

TArray<FPawnAttribute> AInteractiveBaseObject::GetAttributes()
{
	return RequiredSkills;
}

void AInteractiveBaseObject::PauseSynchronizedAnimation()
{
	if (AnimationTimeline != nullptr)
		AnimationTimeline->Deactivate();
}

void AInteractiveBaseObject::ResumeSynchronizedAnimation()
{
	if (AnimationTimeline != nullptr)
		AnimationTimeline->Activate();
}

bool AInteractiveBaseObject::IsFunctionalityActive()
{
	return !isActive;
}

bool AInteractiveBaseObject::IsActivated()
{
	return isActivated;
}

void AInteractiveBaseObject::SetActivation(bool bActivate)
{
	bIsManuallySet = true;
	if (AnimationTimeline != nullptr)
		AnimationTimeline->Stop();
	if (SoundComponent != nullptr)
		SoundComponent->Stop();
	isActivated = bActivate;
	AnimationTimelineFloatReturn(LastAnimationTimelineValue);
}