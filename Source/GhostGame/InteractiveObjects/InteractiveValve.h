// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "InteractiveBaseObject.h"
#include "InteractiveValve.generated.h"

UCLASS()
class GHOSTGAME_API AInteractiveValve : public AInteractiveBaseObject
{
	GENERATED_BODY()

	FRotator OriginalRotation;

	UFUNCTION()
		void AnimationTimelineFloatReturn(float val) override;

	FVector RelativeDisplayLocation;
public:
	//Components
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent *ValveMeshComponent;

	//Properties
	//How many degrees should the valve turn during the animation
	UPROPERTY(EditAnywhere)
		float RotationAngle = 360 * 3;

	//Functions
	AInteractiveValve();
	virtual void BeginPlay() override;
	virtual FVector GetDisplayLocation() override;
};
