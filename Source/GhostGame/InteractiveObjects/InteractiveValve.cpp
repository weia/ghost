// Author: Anna Kobak

#include "GhostGame.h"
#include "InteractiveValve.h"


// Sets default values
AInteractiveValve::AInteractiveValve()
{
	PrimaryActorTick.bCanEverTick = false;

	//Valve Mesh
	ValveMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ValveMeshComponent"));
	static ConstructorHelpers::FObjectFinder<UStaticMesh> ValveStaticMesh(TEXT("StaticMesh'/Game/Meshes/Objects/Object_Valve.Object_Valve'"));
	static ConstructorHelpers::FObjectFinder<UMaterial> ValveMaterial(TEXT("Material'/Game/StarterContent/Materials/M_Metal_Burnished_Steel.M_Metal_Burnished_Steel'"));
	ValveMeshComponent->SetStaticMesh(ValveStaticMesh.Object);
	ValveMeshComponent->SetMaterial(0, ValveMaterial.Object);
	ValveMeshComponent->SetWorldScale3D(FVector(0.5f, 0.5f, 0.5f));
	ValveMeshComponent->AttachTo(RootComponent);

	AddActorWorldRotation(FRotator(-90.0f, 0.0f, 0.0f));
	
	//Other
	DisplayableName = "Valve";
	TimeToCompleteAction = 3;
	TimelinePath = TEXT("CurveFloat'/Game/Blueprints/Timelines/ValveAnimationCurveBP.ValveAnimationCurveBP'");
	CreateTimeline();

	bStopSoundAtAnimationFinished = true;
	SoundPath = TEXT("SoundWave'/Game/Audio/57626__juskiddink__metallic-loop.57626__juskiddink__metallic-loop'");
	CreateSound();

	OriginalRotation = ValveMeshComponent->RelativeRotation;

}

// Called when the game starts or when spawned
void AInteractiveValve::BeginPlay()
{
	Super::BeginPlay();

	FVector Origin, TracedActorBounds;
	Cast<AActor>(this)->GetActorBounds(true, Origin, TracedActorBounds);
	RelativeDisplayLocation = FVector(0.0f, 0.0f, TracedActorBounds.Z + 50.0f);
}

FVector AInteractiveValve::GetDisplayLocation()
{
	return ValveMeshComponent->GetComponentLocation() + RelativeDisplayLocation;
}


void AInteractiveValve::AnimationTimelineFloatReturn(float val)
{
	Super::AnimationTimelineFloatReturn(val);

	if (!isActivated)
		ValveMeshComponent->SetRelativeRotation(OriginalRotation + FRotator(0.0f, RotationAngle*(1.0f - val), 0.0f));
	else
		ValveMeshComponent->SetRelativeRotation(OriginalRotation + FRotator(0.0f, RotationAngle*val, 0.0f));
}
