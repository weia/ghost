// Author: Anna Kobak

#include "GhostGame.h"
#include "InteractiveButton.h"
#include "../Interfaces/PawnInterface.h"

// Sets default values
AInteractiveButton::AInteractiveButton()
{
	PrimaryActorTick.bCanEverTick = false;

	//Frame Mesh
	FrameMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("FrameMeshComponent"));
	static ConstructorHelpers::FObjectFinder<UStaticMesh> FrameStaticMesh(TEXT("StaticMesh'/Game/StarterContent/Shapes/Shape_Cube.Shape_Cube'"));
	static ConstructorHelpers::FObjectFinder<UMaterial> FrameMaterial(TEXT("Material'/Game/StarterContent/Materials/M_Concrete_Panels.M_Concrete_Panels'"));
	FrameMeshComponent->SetStaticMesh(FrameStaticMesh.Object);
	FrameMeshComponent->SetMaterial(0, FrameMaterial.Object);
	FrameMeshComponent->SetRelativeScale3D(FVector(0.5, 0.25, 0.5));
	FVector Location = FrameMeshComponent->GetComponentLocation() + FVector(0.0f, 0.0f, -25.0f);
	FrameMeshComponent->SetWorldLocation(Location);
	FrameMeshComponent->AttachTo(RootComponent);

	//Button Mesh
	ButtonMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ButtonMeshComponent"));
	static ConstructorHelpers::FObjectFinder<UStaticMesh> ButtonStaticMesh(TEXT("StaticMesh'/Game/StarterContent/Shapes/Shape_Cube.Shape_Cube'"));
	static ConstructorHelpers::FObjectFinder<UMaterial> ButtonMaterial(TEXT("Material'/Game/StarterContent/Materials/M_Concrete_Grime.M_Concrete_Grime'"));
	ButtonMeshComponent->SetStaticMesh(ButtonStaticMesh.Object);
	ButtonMeshComponent->SetMaterial(0, ButtonMaterial.Object);
	ButtonMeshComponent->SetRelativeScale3D(FVector(0.25, 0.25, 0.25));
	Location = ButtonMeshComponent->GetComponentLocation() + FVector(0.0f, 5.0f, -12.5f);
	ButtonMeshComponent->SetWorldLocation(Location);
	ButtonMeshComponent->AttachTo(RootComponent);

	//Others
	DisplayableName = "Button";

	TimelinePath = TEXT("CurveFloat'/Game/Blueprints/Timelines/ButtonAnimationCurveBP.ButtonAnimationCurveBP'");
	CreateTimeline();

	SoundPath = TEXT("SoundWave'/Game/Audio/Toggle-SoundBible_com-231290292.Toggle-SoundBible_com-231290292'");
	CreateSound();

	OriginalLocation = ButtonMeshComponent->RelativeLocation;
	LastAnimationTimelineValue = 0.0f;
}


// Called when the game starts or when spawned
void AInteractiveButton::BeginPlay()
{
	Super::BeginPlay();
}


void AInteractiveButton::AnimationTimelineFloatReturn(float val)
{
	Super::AnimationTimelineFloatReturn(val);
	//Animation of button being pressed
	FVector ValueToAdd = FVector(0.0f, -4.5f, 0.0f) * val;
	ButtonMeshComponent->SetRelativeLocation(OriginalLocation + ValueToAdd);
}
