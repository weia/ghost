// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "InteractiveBaseObject.h"
#include "InteractiveLock.generated.h"

UCLASS()
class GHOSTGAME_API AInteractiveLock : public AInteractiveBaseObject
{
	GENERATED_BODY()

public:

	//Components
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* FrameMeshComponent;

	//Functions
	AInteractiveLock();
	virtual void BeginPlay() override;

	//Inherited from IInteractive
	bool IsInteractEnabled(AActor *actor, FInteractCapability* output) override;
	void Interact(AActor* actor) override;
};
