// Author: Grzegorz Bukowiec

#include "GhostGame.h"
#include "LevelPassage.h"


// Sets default values
ALevelPassage::ALevelPassage()
{
 	// Set this actor not to call Tick() every frame
	PrimaryActorTick.bCanEverTick = false;
	SetActorEnableCollision(true);

	//Root Component
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));

	// Create door mesh
	Door = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Door"));
	static ConstructorHelpers::FObjectFinder<UStaticMesh> DoorAsset(TEXT("/Game/StarterContent/Shapes/Shape_Cube"));
	Door->SetStaticMesh(DoorAsset.Object);
	Door->SetWorldScale3D(FVector(2.0f, 0.05f, 3.0f));
	Door->SetWorldLocation(FVector(0.0f, 0.0f, -150.0f));
	Door->AttachTo(RootComponent);

	// Set material
	static ConstructorHelpers::FObjectFinder<UMaterialInterface> MaterialAsset(TEXT("/Game/StarterContent/Materials/M_Basic_WallBlack"));
	class UMaterialInstanceDynamic* DynamicMaterial = UMaterialInstanceDynamic::Create(MaterialAsset.Object, Door);
	Door->SetMaterial(0, DynamicMaterial);
}

// Called when the game starts or when spawned
void ALevelPassage::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ALevelPassage::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

// Interactive functions
void ALevelPassage::Interact(AActor* actor)
{
	Cast<AGhostCharacter>(actor)->FadeIn();
	FTimerHandle UnusedHandle;
	GetWorld()->GetTimerManager().SetTimer(UnusedHandle, this, &ALevelPassage::EnterLevel, 1.5f, false);
}

bool ALevelPassage::IsInteractEnabled(AActor* actor, FInteractCapability* output)
{
	if (Cast<AGhostCharacter>(actor) != nullptr)
	{
		return true;
	}
	else
	{
		output->CanPawnInteract = false;
		return false;
	}
}

// Displayable functions
FString ALevelPassage::GetNameToDisplay()
{
	return DisplayableName;
}

void ALevelPassage::EnterLevel()
{
	UGameplayStatics::OpenLevel(GetWorld(), NewLevelName);
}