// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "../Interfaces/Interactive.h"
#include "../Interfaces/Displayable.h"
#include "../Interfaces/Functional.h"
#include "../Interfaces/PawnInterface.h"
#include "../Utilities/FunctionalObjectsArray.h"
#include "../Utilities/PawnAttribute.h"
#include "../Utilities/InteractCapability.h"
#include "InteractiveBaseObject.generated.h"

UCLASS()
class GHOSTGAME_API AInteractiveBaseObject : public AActor, public IInteractive, public IDisplayable, public IFunctional
{
	GENERATED_BODY()
private:
	bool bIsTimelineCreated = false;

protected:

	void CreateSound();
	
	float LastAnimationTimelineValue = 1.0f;

	bool bIsManuallySet = false;

	FFunctionalObjectsArray FunctionalObjectsStructure;
	const wchar_t* TimelinePath;
	const wchar_t* SoundPath;

	//Animation Timeline related variables
	UPROPERTY()
		UTimelineComponent* AnimationTimeline;
	UPROPERTY()
		UCurveFloat *AnimationCurve;
	FOnTimelineFloat InterpFunction;
	FOnTimelineEvent TimelineFinishedEvent;

	//Animation Timeline related Functions
	void CreateTimeline();

	UFUNCTION()
		virtual void AnimationTimelineFloatReturn(float val) override;

	UFUNCTION()
		virtual void TimelineFinishedFunction();

	bool isActivated = false;
	bool bIsAnimationRunning = false;

	void PauseSynchronizedAnimation() override;
	void ResumeSynchronizedAnimation() override;

public:
	//Properties
	UPROPERTY(EditAnywhere)
		UAudioComponent* SoundComponent;

	UPROPERTY(EditAnywhere)
		bool bIsSoundEnabled = true;

	UPROPERTY(EditAnywhere)
		bool bStopSoundAtAnimationFinished = false;

	//Determine, whether object is activeted at the beginning
	UPROPERTY(EditAnywhere)
		bool isActiveAtStart = false;

	//Determines the name of an object
	//This name will be displayed to player
	UPROPERTY(EditAnywhere)
		FString DisplayableName;

	//Determines whether the object can be interacted with
	UPROPERTY(EditAnywhere)
		bool isActive = true;

	// Specifies whether the object should call Deactivate function when it is pressed second time.
	// If set to true, the object will call ActivateFunctionality and DeactivateFunctionality
	// If set to false, the object will call ActivateFunctionality every time it is pressed.
	UPROPERTY(EditAnywhere)
		bool isDeactivateEnabled = true;

	// How long should it take to complete animation(in seconds)
	// The button object cant be interacted with again until all previous actions are completed
	UPROPERTY(EditAnywhere)
		float TimeToCompleteAction = 1.0f;

	// List of characters which can interact with this object with, default is Zombie
	// The names from this list are compared with GetInteractiveName returned from pawn.
	UPROPERTY(EditAnywhere)
		TArray<FString> InteractWithCharacters;

	//Determines which skills and how high are needed to interact with this object
	UPROPERTY(EditAnywhere)
		TArray<FPawnAttribute> RequiredSkills;

	// List of objects the object should call when it is interacted with.
	// Objects must implement IFunctional in order to be successfuly called.
	UPROPERTY(EditAnywhere)
		TArray<FFunctionalPair> FunctionalObjects;


	AInteractiveBaseObject();
	virtual void BeginPlay() override;
	virtual void Tick( float DeltaSeconds ) override;

	//Inherited from IDisplayable
	TArray<FPawnAttribute> GetAttributes() override;
	virtual FString GetNameToDisplay() override;

	//Inherited from IFunctional
	virtual void ActivateFunctionality(bool bIsAnimationSynchronized, bool bShowAnimation = true) override;
	virtual void DeactivateFunctionality(bool bIsAnimationSynchronized, bool bShowAnimation = true) override;
	virtual bool IsFunctionalityActive() override;

	//Inherited from IInteractive
	virtual bool IsInteractEnabled(AActor *actor, FInteractCapability* output) override;
	virtual void Interact(AActor* actor) override;
	virtual bool IsActivated() override;
	virtual void SetActivation(bool bActivate) override;


};
