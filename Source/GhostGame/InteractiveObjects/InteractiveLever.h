// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "InteractiveBaseObject.h"
#include "InteractiveLever.generated.h"

UCLASS()
class GHOSTGAME_API AInteractiveLever : public AInteractiveBaseObject
{
	GENERATED_BODY()

	FVector OriginalLocation;
	FRotator OriginalRotation;

	UFUNCTION()
		void AnimationTimelineFloatReturn(float val) override;

public:
	//Components
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent *FrameMeshComponent1;
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent *FrameMeshComponent2;
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent *LeverMeshComponent;

	//Functions
	AInteractiveLever();
	virtual void BeginPlay() override;
};
