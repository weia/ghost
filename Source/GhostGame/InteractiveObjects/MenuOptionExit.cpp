// Author: Grzegorz Bukowiec

#include "GhostGame.h"
#include "MenuOptionExit.h"

void AMenuOptionExit::Interact(AActor* actor)
{
	GetWorld()->GetFirstPlayerController()->ConsoleCommand("quit");
}


