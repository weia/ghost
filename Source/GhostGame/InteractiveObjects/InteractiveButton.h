// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "InteractiveBaseObject.h"
#include "InteractiveButton.generated.h"

UCLASS()
class GHOSTGAME_API AInteractiveButton : public AInteractiveBaseObject
{
	GENERATED_BODY()

	FVector OriginalLocation;

	UFUNCTION()
		void AnimationTimelineFloatReturn(float val) override;

public:
	//Components:
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent *FrameMeshComponent;
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent *ButtonMeshComponent;

	//Functions
	AInteractiveButton();
	virtual void BeginPlay() override;
};
