// Author: Grzegorz Bukowiec

#include "GhostGame.h"
#include "MenuOption.h"

AMenuOption::AMenuOption()
{
	TextRenderComponent = CreateDefaultSubobject<UTextRenderComponent>(TEXT("Text"));
	RootComponent = TextRenderComponent;
	TextRenderComponent->SetWorldSize(300.0f);
	TextRenderComponent->VerticalAlignment = EVerticalTextAligment::EVRTA_TextCenter;
	TextRenderComponent->HorizontalAlignment = EHorizTextAligment::EHTA_Center;

	static ConstructorHelpers::FObjectFinder<UMaterialInterface> MaterialAsset(TEXT("/Game/Materials/EmissiveText/M_EmissiveText"));
	TextMaterial = MaterialAsset.Object;

	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("Box"));
	BoxComponent->AttachTo(RootComponent);
	BoxComponent->SetCollisionProfileName(TEXT("BlockAll"));
}

void AMenuOption::BeginPlay()
{
	Super::BeginPlay();
	
	TextRenderComponent->SetText(FText::FromString(CommandName));
	TextRenderComponent->UpdateBounds();
	FVector TextBounds = TextRenderComponent->Bounds.BoxExtent;
	BoxComponent->SetBoxExtent(TextBounds);

	UMaterialInstanceDynamic* DynamicMaterial = UMaterialInstanceDynamic::Create(TextMaterial, TextRenderComponent);
	DynamicMaterial->SetScalarParameterValue(FName("Intensity"), 100.0f);
	TextRenderComponent->SetMaterial(0, DynamicMaterial);
}

FString AMenuOption::GetNameToDisplay()
{
	return "";
}

void AMenuOption::Interact(AActor* actor)
{

}
