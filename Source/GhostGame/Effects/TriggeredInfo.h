// Trigger activated by a pawn that makes it stop and display a text message

#pragma once

#include "GameFramework/Actor.h"
#include "../Interfaces/PawnInterface.h"
#include "TriggeredInfo.generated.h"

UCLASS()
class GHOSTGAME_API ATriggeredInfo : public AActor
{
	GENERATED_BODY()

public:	
	// Sets default values for this actor's properties
	ATriggeredInfo();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
		TArray<FString> OverlapWithCharacters;
	UPROPERTY(EditAnywhere)
		FText MessageText;
	UBoxComponent* Trigger;

	UFUNCTION()
		virtual void Show(AActor* Other);
};
