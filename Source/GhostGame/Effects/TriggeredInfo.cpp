// Author: Grzegorz Bukowiec

#include "GhostGame.h"
#include "TriggeredInfo.h"


// Sets default values
ATriggeredInfo::ATriggeredInfo()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	SetActorEnableCollision(true);

	Trigger = CreateDefaultSubobject<UBoxComponent>(TEXT("Trigger"));
	RootComponent = Trigger;
	Trigger->InitBoxExtent(FVector(5.0f, 500.0f, 500.0f));
	Trigger->SetCollisionProfileName(TEXT("Trigger"));

	OverlapWithCharacters.Add("Ghost");
	OverlapWithCharacters.Add("Zombie");
}

// Called when the game starts or when spawned
void ATriggeredInfo::BeginPlay()
{
	Super::BeginPlay();

	OnActorBeginOverlap.AddDynamic(this, &ATriggeredInfo::Show);
	OnActorEndOverlap.AddDynamic(this, &ATriggeredInfo::Show);
}

void ATriggeredInfo::Show(AActor* Other)
{
	IPawnInterface* PawnInterface = Cast<IPawnInterface>(Other);

	if (PawnInterface && OverlapWithCharacters.Contains(PawnInterface->GetInteractiveName()))
	{
		PawnInterface->DisplayInfo(MessageText.ToString());
		Destroy();
	}
}