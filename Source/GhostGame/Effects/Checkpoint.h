// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "../Global/SaveGameUtility.h"
#include "../Interfaces/Functional.h"
#include "../Interfaces/Interactive.h"
#include "../Characters/GhostCharacter.h"
#include "../Characters/ZombieCharacter.h"
#include "../FunctionalObjects/FunctionalCountingObject.h"
#include "TriggeredInfo.h"
#include "MusicManager.h"
#include "Checkpoint.generated.h"

UCLASS()
class GHOSTGAME_API ACheckpoint : public ATriggeredInfo
{
	GENERATED_BODY()

public:
	ACheckpoint();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
		void Save(AActor* Other);
};
