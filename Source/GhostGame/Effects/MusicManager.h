// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "MusicManager.generated.h"

UCLASS()
class GHOSTGAME_API AMusicManager : public AActor
{
	GENERATED_BODY()

private:
	int soundIndex = 0;

public:	
	// Sets default values for this actor's properties
	AMusicManager();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	UPROPERTY(EditAnywhere)
		UAudioComponent* SoundComponent;
	UPROPERTY(EditAnywhere)
		TArray<USoundBase*> Sounds;

	void ChangeMusic(int newSoundIndex);
	int GetPlayedSoundIndex();
	void SetPlayedSoundIndex(int newSoundIndex);

	UFUNCTION()
		void MusicManagerTimelineFloatReturn(float val);
	UFUNCTION()
		void TimelineFinishedFunction();

	UPROPERTY()
		UTimelineComponent* MusicManagerTimeline;
	UPROPERTY()
		UCurveFloat* MusicManagerCurve;
	FOnTimelineFloat InterpFunction{};
	FOnTimelineEvent TimelineFinishedEvent;
};
