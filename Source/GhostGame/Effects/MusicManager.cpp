// Author: Grzegorz Bukowiec

#include "GhostGame.h"
#include "MusicManager.h"


// Sets default values
AMusicManager::AMusicManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SoundComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("SoundComponent"));
	RootComponent = SoundComponent;
	SoundComponent->SetRelativeLocation(FVector(0, 0, 0));

	ConstructorHelpers::FObjectFinder<UCurveFloat> Curve(TEXT("CurveFloat'/Game/Blueprints/Timelines/MusicManagerCurveBP.MusicManagerCurveBP'"));
	MusicManagerCurve = Curve.Object;
	MusicManagerTimeline = CreateDefaultSubobject<UTimelineComponent>(TEXT("MusicManagerTimeline"));
	MusicManagerTimeline->SetTimelineLength(2.2f);
	InterpFunction.BindUFunction(this, FName{ TEXT("MusicManagerTimelineFloatReturn") });
	TimelineFinishedEvent.BindUFunction(this, FName{ TEXT("TimelineFinishedFunction") });
}

// Called when the game starts or when spawned
void AMusicManager::BeginPlay()
{
	Super::BeginPlay();
	SoundComponent->SetSound(Sounds[0]);
	SoundComponent->Play();

	MusicManagerTimeline->SetTimelineFinishedFunc(TimelineFinishedEvent);
	MusicManagerTimeline->AddInterpFloat(MusicManagerCurve, InterpFunction, FName{ TEXT("InterpFloat") });
}

// Called every frame
void AMusicManager::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
}

void AMusicManager::ChangeMusic(int newSoundIndex)
{
	if (soundIndex != newSoundIndex && newSoundIndex < Sounds.Num())
	{
		soundIndex = newSoundIndex;
		MusicManagerTimeline->PlayFromStart();
	}
}

void AMusicManager::MusicManagerTimelineFloatReturn(float val)
{
	SoundComponent->SetVolumeMultiplier(val);
}

void AMusicManager::TimelineFinishedFunction()
{
	SoundComponent->SetSound(Sounds[soundIndex]);
	SoundComponent->SetVolumeMultiplier(1.0f);
	SoundComponent->Play();
}

int AMusicManager::GetPlayedSoundIndex()
{
	return soundIndex;
}

void AMusicManager::SetPlayedSoundIndex(int newSoundIndex)
{
	if (soundIndex != newSoundIndex && newSoundIndex < Sounds.Num())
	{
		SoundComponent->SetVolumeMultiplier(0.0f);
		ChangeMusic(newSoundIndex);
	}
}

