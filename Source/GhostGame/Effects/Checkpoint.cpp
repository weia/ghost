// Author: Grzegorz Bukowiec

#include "GhostGame.h"
#include "Checkpoint.h"

ACheckpoint::ACheckpoint()
{
	MessageText = FText::FromString("Progress saved");
}

// Called when the game starts or when spawned
void ACheckpoint::BeginPlay()
{
	OnActorBeginOverlap.AddDynamic(this, &ACheckpoint::Save);
	OnActorEndOverlap.AddDynamic(this, &ACheckpoint::Save);

	Super::BeginPlay();
}

void ACheckpoint::Save(AActor* Other)
{
	Show(Other);

	IPawnInterface* PawnInterface = Cast<IPawnInterface>(Other);
	if (PawnInterface && OverlapWithCharacters.Contains(PawnInterface->GetInteractiveName()))
	{
		USaveGameUtility* SaveGame;
		if (UGameplayStatics::DoesSaveGameExist("save", 0))
		{
			SaveGame = Cast<USaveGameUtility>(UGameplayStatics::LoadGameFromSlot("save", 0));

		}
		else
		{
			SaveGame = Cast<USaveGameUtility>(UGameplayStatics::CreateSaveGameObject(USaveGameUtility::StaticClass()));
		}
		SaveGame->LevelName = GetWorld()->GetMapName().Right(6);
		SaveGame->PawnName = Other->GetName();
		SaveGame->PawnLocation = Other->GetActorLocation();
		SaveGame->ActorPositions.Empty();
		SaveGame->FunctionalStates.Empty();
		SaveGame->InteractiveStates.Empty();
		SaveGame->TriggersLeft.Empty();
		SaveGame->CountingObjectValues.Empty();

		IFunctional* FunctionalActor;
		IInteractive* InteractiveActor;
		AGhostCharacter* GhostActor;
		AZombieCharacter* ZombieActor;
		AFunctionalCountingObject* FunctionalCoutingObjectActor;
		ATriggeredInfo* TriggeredInfoActor;
		AMusicManager* MusicManagerActor;

		for (AActor* Actor : GetWorld()->GetCurrentLevel()->Actors)
		{
			if (Actor != nullptr)
			{
				FunctionalActor = Cast<IFunctional>(Actor);
				InteractiveActor = Cast<IInteractive>(Actor);
				GhostActor = Cast<AGhostCharacter>(Actor);
				ZombieActor = Cast<AZombieCharacter>(Actor);
				FunctionalCoutingObjectActor = Cast<AFunctionalCountingObject>(Actor);
				TriggeredInfoActor = Cast<ATriggeredInfo>(Actor);
				MusicManagerActor = Cast<AMusicManager>(Actor);

				if (FunctionalActor != nullptr)
				{
					SaveGame->FunctionalStates.Add(Actor->GetName(), FunctionalActor->IsFunctionalityActive() ? 1 : 0);
				}
				if (InteractiveActor != nullptr)
				{
					SaveGame->InteractiveStates.Add(Actor->GetName(), InteractiveActor->IsActivated() ? 1 : 0);
				}
				if (GhostActor == nullptr && FunctionalActor == nullptr && (InteractiveActor == nullptr || ZombieActor != nullptr))
				{
					SaveGame->ActorPositions.Add(Actor->GetName(), FActorData(Actor->GetActorLocation(), Actor->GetActorRotation()));
				}
				if (FunctionalCoutingObjectActor != nullptr)
				{
					SaveGame->CountingObjectValues.Add(Actor->GetName(), FunctionalCoutingObjectActor->GetNumberOfActivations());
				}
				if (TriggeredInfoActor != nullptr)
				{
					SaveGame->TriggersLeft.Add(Actor->GetName());
				}
				if (MusicManagerActor != nullptr)
				{
					SaveGame->SoundIndex = MusicManagerActor->GetPlayedSoundIndex();
				}
			}
		}

		SaveGame->TriggersLeft.Remove(GetName());

		UGameplayStatics::SaveGameToSlot(SaveGame, "save", 0);

		Destroy();
	}
}