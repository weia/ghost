// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "MusicManager.h"
#include "MusicTrigger.generated.h"

UCLASS()
class GHOSTGAME_API AMusicTrigger : public AActor
{
	GENERATED_BODY()

public:	
	// Sets default values for this actor's properties
	AMusicTrigger();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	UPROPERTY(EditAnywhere)
		AMusicManager* MusicManager;
	UPROPERTY(EditAnywhere)
		TArray<FString> OverlapWithCharacters;
	UPROPERTY(EditAnywhere)
		int32 soundIndex;
	UBoxComponent* Trigger;	

	UFUNCTION()
		void ChangeMusic(AActor* Other);
};
