// Author: Grzegorz Bukowiec

#include "GhostGame.h"
#include "../Interfaces/PawnInterface.h"
#include "MusicTrigger.h"


// Sets default values
AMusicTrigger::AMusicTrigger()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SetActorEnableCollision(true);

	Trigger = CreateDefaultSubobject<UBoxComponent>(TEXT("Trigger"));
	RootComponent = Trigger;
	Trigger->InitBoxExtent(FVector(5.0f, 500.0f, 500.0f));
	Trigger->SetCollisionProfileName(TEXT("Trigger"));

	OverlapWithCharacters.Add("Ghost");
	OverlapWithCharacters.Add("Zombie");
}

// Called when the game starts or when spawned
void AMusicTrigger::BeginPlay()
{
	Super::BeginPlay();
	
	OnActorBeginOverlap.AddDynamic(this, &AMusicTrigger::ChangeMusic);
}

// Called every frame
void AMusicTrigger::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
}

void AMusicTrigger::ChangeMusic(AActor* Other)
{
	if (Cast<IPawnInterface>(Other) != nullptr && OverlapWithCharacters.Contains(Cast<IPawnInterface>(Other)->GetInteractiveName()))
	{
		MusicManager->ChangeMusic(soundIndex);
	}
}

