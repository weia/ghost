// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "FunctionalBaseObject.h"
#include "FunctionalPlatform.generated.h"

UCLASS()
class GHOSTGAME_API AFunctionalPlatform : public AFunctionalBaseObject
{
	GENERATED_BODY()
	
	FVector OriginalLocation;
	//Animation Timeline related variables

	void SetActivatedMeshPosition() override;
	void SetDeactivatedMeshPosition() override;

public:	
	//Components
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent *PlatformMeshComponent;

	UPROPERTY(EditAnywhere)
		UBoxComponent* BoundingBox;

	//Properties

	//How far the platorm should be moved(scale)
	UPROPERTY(EditAnywhere)
		float MovementLength = 100.0f;
	//Direction(s) in which platform should be moved
	UPROPERTY(EditAnywhere)
		FVector MovementDirection = FVector(0.0f, 0.0f, 1.0f);

	//Functions
	AFunctionalPlatform();
	virtual void BeginPlay() override;
	virtual void PostLoad() override;

	UFUNCTION()
		void AnimationTimelineFloatReturn(float val) override;
};
