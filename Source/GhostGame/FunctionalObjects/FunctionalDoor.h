// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "FunctionalBaseObject.h"
#include "FunctionalDoor.generated.h"

UCLASS()
class GHOSTGAME_API AFunctionalDoor : public AFunctionalBaseObject
{
	GENERATED_BODY()
	int OpeningDirection;
	FRotator OriginalRotation;

	void SetActivatedMeshPosition() override;
	void SetDeactivatedMeshPosition() override;

public:
	//Components
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent *StaticMeshComponent;

	UPROPERTY(EditAnywhere)
		UBoxComponent* BoundingBox;


	//Properties

	//How wide door should open
	UPROPERTY(EditAnywhere)
		float OpeningAngle = 90.0f;
	//Whether the door should open clockwise(true) or counterclockwise(false)
	UPROPERTY(EditAnywhere)
		bool openClockwise = true;

	//Functions
	AFunctionalDoor();
	virtual void BeginPlay() override;
	virtual void PostLoad() override;

	UFUNCTION()
		void AnimationTimelineFloatReturn(float val) override;

};
