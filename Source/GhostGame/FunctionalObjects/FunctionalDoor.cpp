/// Author: Anna Kobak

#include "GhostGame.h"
#include "FunctionalDoor.h"


// Sets default values
AFunctionalDoor::AFunctionalDoor()
{
	PrimaryActorTick.bCanEverTick = true;

	//Mesh
	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	static ConstructorHelpers::FObjectFinder<UStaticMesh> StaticMesh(TEXT("StaticMesh'/Game/StarterContent/Architecture/Wall_400x300.Wall_400x300'"));
	static ConstructorHelpers::FObjectFinder<UMaterial> Material(TEXT("Material'/Game/StarterContent/Materials/M_Metal_Rust.M_Metal_Rust'"));

	StaticMeshComponent->SetStaticMesh(StaticMesh.Object);
	StaticMeshComponent->SetMaterial(0, Material.Object);
	StaticMeshComponent->AttachTo(RootComponent);

	BoundingBox = CreateDefaultSubobject<UBoxComponent>(TEXT("Trigger"));
	BoundingBox->InitBoxExtent(FVector(50.0f, 50.0f, 50.0f));
	BoundingBox->SetCollisionResponseToAllChannels(ECR_Ignore);
	BoundingBox->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);
	BoundingBox->SetCollisionResponseToChannel(ECollisionChannel::ECC_GameTraceChannel1, ECollisionResponse::ECR_Overlap);
	BoundingBox->AttachTo(StaticMeshComponent);
	BoundingBox->SetRelativeScale3D(FVector(4.02f, 0.22f, 3.02f));
	BoundingBox->SetRelativeLocation(FVector(200.0f, 0.0f, 150.0f));

	OnActorBeginOverlap.AddDynamic(this, &AFunctionalDoor::BoundingBoxBeginOverlap);
	OnActorEndOverlap.AddDynamic(this, &AFunctionalDoor::BoundingBoxEndOverlap);


//	OriginalRotation = RootComponent->RelativeRotation;
	TimelinePath = TEXT("CurveFloat'/Game/Blueprints/Timelines/DoorAnimationCurveBP.DoorAnimationCurveBP'");
	Super::CreateTimeline();

	SoundPath = TEXT("SoundWave'/Game/Audio/Old_Door_Creaking-SoundBible_com-1197162460.Old_Door_Creaking-SoundBible_com-1197162460'");
	CreateSound();
}

void AFunctionalDoor::PostLoad()
{
	Super::PostLoad();
	OriginalRotation = RootComponent->RelativeRotation;
	OpeningDirection = openClockwise ? 1 : -1;
}

// Called when the game starts or when spawned
void AFunctionalDoor::BeginPlay()
{
	Super::BeginPlay();
}


void AFunctionalDoor::AnimationTimelineFloatReturn(float val)
{
	if (!isFunctionalityActive)
		RootComponent->SetRelativeRotation(OriginalRotation + FRotator(0.0f, OpeningDirection*OpeningAngle*(1.0f-val), 0.0f));
	else
		RootComponent->SetRelativeRotation(OriginalRotation + FRotator(0.0f, OpeningDirection*OpeningAngle*val, 0.0f));
}

void AFunctionalDoor::SetActivatedMeshPosition()
{
	RootComponent->SetRelativeRotation(OriginalRotation + FRotator(0.0f, OpeningDirection*OpeningAngle, 0.0f));
}

void AFunctionalDoor::SetDeactivatedMeshPosition()
{
	RootComponent->SetRelativeRotation(OriginalRotation);
}