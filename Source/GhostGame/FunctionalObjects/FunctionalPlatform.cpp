// Author: Anna Kobak

#include "GhostGame.h"
#include "FunctionalPlatform.h"


// Sets default values
AFunctionalPlatform::AFunctionalPlatform()
{
	PrimaryActorTick.bCanEverTick = true;

	//Platform Mesh
	PlatformMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PlatformMesh"));
	static ConstructorHelpers::FObjectFinder<UStaticMesh> StaticMesh(TEXT("StaticMesh'/Game/StarterContent/Architecture/Floor_400x400.Floor_400x400'"));
	static ConstructorHelpers::FObjectFinder<UMaterial> Material(TEXT("Material'/Game/StarterContent/Materials/M_Concrete_Poured.M_Concrete_Poured'"));
	PlatformMeshComponent->SetStaticMesh(StaticMesh.Object);
	PlatformMeshComponent->SetMaterial(0, Material.Object);
	PlatformMeshComponent->AttachTo(RootComponent);

	//Animation Timeline
	TimelinePath = TEXT("CurveFloat'/Game/Blueprints/Timelines/PlatformAnimationCurveBP.PlatformAnimationCurveBP'");
	CreateTimeline();

	BoundingBox = CreateDefaultSubobject<UBoxComponent>(TEXT("Trigger"));
	BoundingBox->InitBoxExtent(FVector(50.0f, 50.0f, 50.0f));
	BoundingBox->SetCollisionResponseToAllChannels(ECR_Ignore);
	BoundingBox->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);
	BoundingBox->SetCollisionResponseToChannel(ECollisionChannel::ECC_GameTraceChannel1, ECollisionResponse::ECR_Overlap);
	BoundingBox->AttachTo(PlatformMeshComponent);
	BoundingBox->SetRelativeScale3D(FVector(4.02f, 4.02, 0.2));
	BoundingBox->SetRelativeLocation(FVector(200.0f, 200.0f, -10.0f));

	OnActorBeginOverlap.AddDynamic(this, &AFunctionalPlatform::BoundingBoxBeginOverlap);
	OnActorEndOverlap.AddDynamic(this, &AFunctionalPlatform::BoundingBoxEndOverlap);

	SoundPath = TEXT("SoundWave'/Game/Audio/Sliding-SoundBible_com-649905012.Sliding-SoundBible_com-649905012'");
	CreateSound();
}

void AFunctionalPlatform::PostLoad()
{
	Super::PostLoad();
	OriginalLocation = RootComponent->RelativeLocation;
}

// Called when the game starts or when spawned
void AFunctionalPlatform::BeginPlay()
{
	Super::BeginPlay();
}


void AFunctionalPlatform::AnimationTimelineFloatReturn(float val)
{
	FVector ValueToAdd = MovementDirection*MovementLength;
	ValueToAdd *= isFunctionalityActive ? val : 1.0f - val;
	RootComponent->SetRelativeLocation(OriginalLocation + ValueToAdd);
}


void AFunctionalPlatform::SetActivatedMeshPosition()
{
	RootComponent->SetRelativeLocation(OriginalLocation + MovementDirection*MovementLength);
}

void AFunctionalPlatform::SetDeactivatedMeshPosition()
{
	RootComponent->SetRelativeLocation(OriginalLocation);
}