// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "../Interfaces/Functional.h"
#include "../Interfaces/Interactive.h"
#include "../Interfaces/PawnInterface.h"
#include "FunctionalBaseObject.generated.h"

UCLASS()
class GHOSTGAME_API AFunctionalBaseObject : public AActor, public IFunctional
{
	GENERATED_BODY()

protected:
	UPROPERTY(EditAnywhere)
		UAudioComponent* SoundComponent;

	UPROPERTY(EditAnywhere)
		bool bIsSoundEnabled = true;

	//Animation Timeline related variables
	UPROPERTY()
		UTimelineComponent* AnimationTimeline;
	UPROPERTY()
		UCurveFloat *AnimationCurve;
	FOnTimelineFloat InterpFunction{};
	FOnTimelineEvent TimelineFinishedEvent;
	IInteractive* AnimationCallerActor;
	
	const wchar_t* TimelinePath;
	const wchar_t* SoundPath;

	UFUNCTION()
		void BoundingBoxBeginOverlap(AActor* Other);
	UFUNCTION()
		void BoundingBoxEndOverlap(AActor* Other);

	FString ScheduledAction;

	int NumberOfOverlapping = 0;


	bool isFunctionalityActive = false;
	//How long should it take to complete animation(in seconds)
	//Nothing can be done with object until all previous actions are completed
	UPROPERTY(EditAnywhere)
		float TimeToCompleteAction = 1.0f;
	//Whether it should be possible to deactivate the object.
	//If set to false, object once activated will remain activated forever
	//(until someone resets it, that is, but calling DeactivateFunctionality won't do it)
	UPROPERTY(EditAnywhere)
		bool isDeactivateFunctionalityEnabled = true;

	//true if someone is performing animation
	bool bIsAnimationRunning = false;
	bool bIsAnimationSynchronized = false;

	//Call to create a timeline
	//Path to the timeline should be set in TimelinePath
	void CreateTimeline();

	void CreateSound();

	UFUNCTION()
		virtual void TimelineFinishedFunction();

	virtual void SetActivatedMeshPosition();
	virtual void SetDeactivatedMeshPosition();

public:	
	AFunctionalBaseObject();
	virtual void BeginPlay() override;
	virtual void Tick( float DeltaSeconds ) override;

	//Inherited from IFunctional
	virtual void ActivateFunctionality(bool bIsAnimationSynchronized, bool bShowAnimation = true) override;
	virtual void DeactivateFunctionality(bool bIsAnimationSynchronized, bool bShowAnimation = true) override;
	virtual void BeginOfAnimation(IInteractive* AActor = nullptr) override;
	virtual void EndOfAnimation() override;
	virtual bool CanActivate() override;
	virtual bool CanDeactivate() override;	
	virtual void ScheduleActivation() override;
	virtual void ScheduleDeactivation() override;
	virtual bool IsFunctionalityActive() override;
};
