// Author: Anna Kobak

#include "GhostGame.h"
#include "FunctionalBaseObject.h"


// Sets default values
AFunctionalBaseObject::AFunctionalBaseObject()
{
	PrimaryActorTick.bCanEverTick = false;

	//RootComponent
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
}

void AFunctionalBaseObject::CreateSound()
{
	SoundComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("SoundComponent"));
	ConstructorHelpers::FObjectFinder<USoundBase> SoundHelper(SoundPath);
	SoundComponent->bAutoActivate = false;
	SoundComponent->SetSound(SoundHelper.Object);
	SoundComponent->AttachTo(RootComponent);
	SoundComponent->SetRelativeLocation(FVector(0, 0, 0));
}


void AFunctionalBaseObject::CreateTimeline()
{
	//Animation Timeline
	ConstructorHelpers::FObjectFinder<UCurveFloat> Curve(TimelinePath);
	AnimationCurve = Curve.Object;
	AnimationTimeline = CreateDefaultSubobject<UTimelineComponent>(TEXT("AnimationTimeline"));
	AnimationTimeline->SetTimelineLength(1.0f);
	InterpFunction.BindUFunction(this, FName{ TEXT("AnimationTimelineFloatReturn") });
	TimelineFinishedEvent.BindUFunction(this, FName{ TEXT("TimelineFinishedFunction") });
}

// Called when the game starts or when spawned
void AFunctionalBaseObject::BeginPlay()
{
	Super::BeginPlay();
	if (AnimationTimeline != nullptr) {
		AnimationTimeline->SetTimelineFinishedFunc(TimelineFinishedEvent);
		AnimationTimeline->SetPlayRate(TimeToCompleteAction == 0 ? 100.0f : 1.0f / TimeToCompleteAction);
		AnimationTimeline->AddInterpFloat(AnimationCurve, InterpFunction, FName{ TEXT("InterpFloat") });
	}
}

// Called every frame
void AFunctionalBaseObject::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
}

void AFunctionalBaseObject::ActivateFunctionality(bool bIsAnimationSynchronized, bool bShowAnimation)
{
	if (!bShowAnimation) {

		bIsAnimationRunning = false;
		ScheduledAction = "";
		if (AnimationTimeline != nullptr)
			AnimationTimeline->Stop();
		if (SoundComponent != nullptr)
			SoundComponent->Stop();
		isFunctionalityActive = true;
		SetActivatedMeshPosition();

	} else if (CanActivate()) {
		isFunctionalityActive = true;
		bIsAnimationRunning = true;
		if (SoundComponent != nullptr && bIsSoundEnabled)
			SoundComponent->Play();
		if (!bIsAnimationSynchronized) {
			AnimationTimeline->PlayFromStart();
		}
		if (ScheduledAction == "Activate")
			ScheduledAction = "";
	}
}

void AFunctionalBaseObject::DeactivateFunctionality(bool bIsAnimationSynchronized, bool bShowAnimation)
{
	if (!bShowAnimation) {
		bIsAnimationRunning = false;
		ScheduledAction = "";
		if(AnimationTimeline != nullptr)
			AnimationTimeline->Stop();
		if (SoundComponent != nullptr)
			SoundComponent->Stop();
		isFunctionalityActive = false;
		SetDeactivatedMeshPosition();
	} else if (CanDeactivate()) {
		isFunctionalityActive = false;
		bIsAnimationRunning = true;
		if (SoundComponent != nullptr && bIsSoundEnabled)
			SoundComponent->Play();
		if (!bIsAnimationSynchronized) {
			AnimationTimeline->PlayFromStart();
		}
		if (ScheduledAction == "Deactivate")
			ScheduledAction = "";
	}
}


void AFunctionalBaseObject::BeginOfAnimation(IInteractive* Actor)
{
	AnimationCallerActor = Actor;
	bIsAnimationSynchronized = true;
	bIsAnimationRunning = true;
}

void AFunctionalBaseObject::EndOfAnimation()
{
	bIsAnimationSynchronized = false;
	bIsAnimationRunning = false;
}

bool AFunctionalBaseObject::CanActivate()
{
	return (!isFunctionalityActive  && !bIsAnimationRunning);
}

bool AFunctionalBaseObject::CanDeactivate()
{
	return (isDeactivateFunctionalityEnabled && isFunctionalityActive && !bIsAnimationRunning);
}


void AFunctionalBaseObject::ScheduleActivation()
{
	if (!isFunctionalityActive && bIsAnimationRunning)
		ScheduledAction = "Activate";
	else
		ScheduledAction = "";
	ActivateFunctionality(false);
}


void AFunctionalBaseObject::ScheduleDeactivation()
{
	if(isDeactivateFunctionalityEnabled && isFunctionalityActive && bIsAnimationRunning)
		ScheduledAction = "Deactivate";
	else
		ScheduledAction = "";
	DeactivateFunctionality(false);
}

void AFunctionalBaseObject::TimelineFinishedFunction()
{
	bIsAnimationRunning = false;
	if (ScheduledAction == "Activate" && isFunctionalityActive == false)
		ActivateFunctionality(false);
	else if (ScheduledAction == "Deactivate" && isFunctionalityActive == true)
		DeactivateFunctionality(false);
	ScheduledAction = "";
}


void AFunctionalBaseObject::BoundingBoxBeginOverlap(AActor* Other)
{
	if (Cast<IPawnInterface>(Other) == nullptr)
		return;
	if (NumberOfOverlapping == 0)
		if (AnimationTimeline != nullptr)
			if (bIsAnimationSynchronized) {
				if (AnimationCallerActor != nullptr)
					AnimationCallerActor->PauseSynchronizedAnimation();
			}
			else
				AnimationTimeline->Deactivate();
	NumberOfOverlapping++;
}

void AFunctionalBaseObject::BoundingBoxEndOverlap(AActor* Other)
{
	if (Cast<IPawnInterface>(Other) == nullptr)
		return;

	NumberOfOverlapping--;
	if (NumberOfOverlapping == 0)
		if (AnimationTimeline != nullptr)
			if (bIsAnimationSynchronized) {
				if (AnimationCallerActor != nullptr)
					AnimationCallerActor->ResumeSynchronizedAnimation();
			}
			else
				AnimationTimeline->Activate();
}

void AFunctionalBaseObject::SetActivatedMeshPosition()
{
}

void AFunctionalBaseObject::SetDeactivatedMeshPosition()
{

}

bool AFunctionalBaseObject::IsFunctionalityActive()
{
	return isFunctionalityActive;
}