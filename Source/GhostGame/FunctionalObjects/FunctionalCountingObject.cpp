// Author: Anna Kobak

#include "GhostGame.h"
#include "FunctionalCountingObject.h"


// Sets default values
AFunctionalCountingObject::AFunctionalCountingObject()
{
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));

	PrimaryActorTick.bCanEverTick = false;
	FunctionalObjectsStructure.SetArray(&FunctionalObjects);
}

// Called when the game starts or when spawned
void AFunctionalCountingObject::BeginPlay()
{
}

void AFunctionalCountingObject::ActivateFunctionality(bool bIsAnimationSynchronized, bool bShowAnimation) {
	if (!bShowAnimation && bIsManuallySet)
		return;

	NumberOfActivations++;
	if (NumberOfActivations == MinimalNumberOfActivations)
		if (bShowAnimation)
			FunctionalObjectsStructure.CallScheduleActivationInAll();
		else
			FunctionalObjectsStructure.CallActivateFunctionalityInAll(nullptr, false);
}

void AFunctionalCountingObject::DeactivateFunctionality(bool bIsAnimationSynchronized, bool bShowAnimation) {
	if (!bShowAnimation && bIsManuallySet)
		return;

	if (NumberOfActivations == MinimalNumberOfActivations)
		if(bShowAnimation)
			FunctionalObjectsStructure.CallScheduleDeactivationInAll();
		else
			FunctionalObjectsStructure.CallDeactivateFunctionalityInAll(nullptr, false);
	NumberOfActivations--;
}

bool AFunctionalCountingObject::IsFunctionalityActive() {
	return NumberOfActivations >= MinimalNumberOfActivations;
}

int AFunctionalCountingObject::GetNumberOfActivations()
{
	return NumberOfActivations;
}

void AFunctionalCountingObject::SetNumberOfActivations(int Number)
{
	bIsManuallySet = true;
	NumberOfActivations = Number;
}
