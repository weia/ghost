// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "../Interfaces/Functional.h"
#include "../Utilities/FunctionalObjectsArray.h"
#include "FunctionalCountingObject.generated.h"

UCLASS()
class GHOSTGAME_API AFunctionalCountingObject : public AActor, public IFunctional
{
	GENERATED_BODY()

	bool bIsManuallySet = false;

	FFunctionalObjectsArray FunctionalObjectsStructure;
	int NumberOfActivations = 0;

public:	
	//When the number of Activate Functionality calls minus number of DeactivateFunctionality calls
	// on this object reaches MinimalNumberOfActivations, then ActivateFunctionality on all FunctionalObjects is called
	//When the the number of Activate Functionality calls minus number of DeactivateFunctionality calls
	// on this object drops below MinimalNumberOfActivations, then ActivateFunctionality on all FunctionalObjects is called
	UPROPERTY(EditAnywhere)
		int32 MinimalNumberOfActivations = 1;

	// List of objects the object should call.
	// Objects must implement IFunctional in order to be successfuly called.
	UPROPERTY(EditAnywhere)
		TArray<FFunctionalPair> FunctionalObjects;

	AFunctionalCountingObject();
	virtual void BeginPlay() override;
	
	void ActivateFunctionality(bool bIsAnimationSynchronized, bool bShowAnimation = true) override;
	void DeactivateFunctionality(bool bIsAnimationSynchronized, bool bShowAnimation = true) override;

	bool IsFunctionalityActive() override;

	int GetNumberOfActivations();
	void SetNumberOfActivations(int Number);
};
