// An object that a ghost can just pass through

#pragma once

#include "GameFramework/Actor.h"
#include "../Interfaces/Interactive.h"
#include "../Interfaces/Displayable.h"
#include "Passable.generated.h"

UCLASS()
class GHOSTGAME_API APassable : public AActor
{
	GENERATED_BODY()

private:
	// Toggle actor's visibility when passed through
	UFUNCTION()
		void Show(AActor* Other);
	UFUNCTION()
		void Hide(AActor* Other);

public:
	// Sets default values for this actor's properties
	APassable();
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	// Called every frame
	virtual void Tick(float DeltaSeconds) override;

	// Object's mesh (probably a wall)
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* StaticMesh;
};