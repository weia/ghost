// Author: Grzegorz Bukowiec

#include "GhostGame.h"
#include "Passable.h"


// Sets default values
APassable::APassable()
{
	// Set this actor not to call Tick() every frame
	PrimaryActorTick.bCanEverTick = false;
	SetActorEnableCollision(true);

	//Create actor's mesh
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisualRepresentation"));
	RootComponent = StaticMesh;
	StaticMesh->SetCollisionProfileName(TEXT("Passable"));
}

// Called when the game starts or when spawned
void APassable::BeginPlay()
{
	Super::BeginPlay();

	// Bind these functions to actor's overlapping
	OnActorBeginOverlap.AddDynamic(this, &APassable::Hide);
	OnActorEndOverlap.AddDynamic(this, &APassable::Show);
}

// Called every frame
void APassable::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Toggle actor's visibility when passed through
void APassable::Hide(AActor* Other)
{
	StaticMesh->SetVisibility(false);
}
void APassable::Show(AActor* Other)
{
	StaticMesh->SetVisibility(true);
}