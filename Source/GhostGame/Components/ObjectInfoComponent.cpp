// Author: Grzegorz Bukowiec
// Component responsible for displaying information about an object that a pawn looks at

#include "GhostGame.h"
#include "../Utilities/PawnAttribute.h"
#include "ObjectInfoComponent.h"


// Sets default values for this component's properties
UObjectInfoComponent::UObjectInfoComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	bWantsBeginPlay = true;
	PrimaryComponentTick.bCanEverTick = true;

	Owner = Cast<IPawnInterface>(GetOwner());

	//Root Component
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));

	// Owner's camera
	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));

	//Create and attach a hidden text
	TextInfo = CreateDefaultSubobject<UTextRenderComponent>(TEXT("Text"));
	TextInfo->SetVisibility(false);
	TextInfo->SetWorldSize(48.0f);
	TextInfo->SetWorldLocation(FVector(0.0f, 0.0f, 10.0f));
	TextInfo->SetWorldRotation(FRotator(0.0f, 180.0f, 0.0f));
	TextInfo->VerticalAlignment = EVerticalTextAligment::EVRTA_TextCenter;
	TextInfo->HorizontalAlignment = EHorizTextAligment::EHTA_Center;
	TextInfo->AttachTo(RootComponent);

	//Set materials
	static ConstructorHelpers::FObjectFinder<UMaterialInterface> TextMaterialAsset(TEXT("/Game/Materials/EmissiveText/M_EmissiveText"));
	TextMaterial = TextMaterialAsset.Object;

	// Create components for object attributes
	static ConstructorHelpers::FObjectFinder<UStaticMesh> TrimShapeAsset(TEXT("/Game/StarterContent/Shapes/Shape_Trim"));
	static ConstructorHelpers::FObjectFinder<UMaterialInterface> BarMaterialAsset(TEXT("/Game/Materials/M_Tech_Hex_Tile_Pulse_Translucent"));
	for (int i = 0; i < MAX_ATTRIBUTES; ++i)
	{
		AttributeComponents[i] = CreateDefaultSubobject<UStaticMeshComponent>(FName(*("Attribute" + FString::FromInt(i))));
		AttributeComponents[i]->SetStaticMesh(TrimShapeAsset.Object);
		AttributeComponents[i]->SetVisibility(false);
		AttributeComponents[i]->SetWorldLocation(FVector(0.0f, 0.0f, (-15.0f) * (i + 1)));
		AttributeComponents[i]->SetWorldScale3D(FVector(0.01f, 1.5f, 0.5f));
		AttributeComponents[i]->AttachTo(RootComponent);
		AttributeComponents[i]->SetCollisionEnabled(ECollisionEnabled::NoCollision);

		UMaterialInstanceDynamic* BarMaterialDynamic = UMaterialInstanceDynamic::Create(BarMaterialAsset.Object, AttributeComponents[i]);
		AttributeComponents[i]->SetMaterial(0, BarMaterialDynamic);

		AttributeTexts[i] = CreateDefaultSubobject<UTextRenderComponent>(FName(*("AttributeText" + FString::FromInt(i))));
		AttributeTexts[i]->SetVisibility(false);
		AttributeTexts[i]->SetWorldSize(16.0f);
		AttributeTexts[i]->SetWorldLocation(FVector(0.0f, -120.0f, 5.0 + (-15.0f) * (i + 1)));
		AttributeTexts[i]->SetWorldRotation(FRotator(0.0f, 180.0f, 0.0f));
		AttributeTexts[i]->VerticalAlignment = EVerticalTextAligment::EVRTA_TextCenter;
		AttributeTexts[i]->HorizontalAlignment = EHorizTextAligment::EHTA_Left;
		AttributeTexts[i]->AttachTo(RootComponent);
	}
}


// Called when the game starts
void UObjectInfoComponent::BeginPlay()
{
	Super::BeginPlay();

	for (UStaticMeshComponent* Component : AttributeComponents)
	{
		Component->SetMassOverrideInKg(NAME_None, 0.0f, true);
	}
}

// Called every frame
void UObjectInfoComponent::TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction )
{
	Super::TickComponent( DeltaTime, TickType, ThisTickFunction );
	// Update text rotation in direction of the player camera
	FRotator CameraRotation = GetWorld()->GetFirstPlayerController()->PlayerCameraManager->GetCameraRotation();
	//RootComponent->SetWorldRotation(FRotator(-CameraRotation.Pitch, 180.0f + CameraRotation.Yaw, -CameraRotation.Roll));
	RootComponent->SetWorldRotation(FRotator(CameraRotation));
	if (TracedObject != nullptr)
	{
		RootComponent->SetWorldLocation(TracedObject->GetDisplayLocation() + FVector(0.0f, 0.0f, TracedObject->GetAttributes().Num() * 15.0f));
	}
}

// Display given info about an object
void UObjectInfoComponent::Display(IDisplayable* TracedActor, FInteractCapability* InteractCapability)
{
	TracedObject = TracedActor;

	TArray<FPawnAttribute> Attributes = TracedActor->GetAttributes();
	for (int i = 0; i < Attributes.Num(); ++i)
	{
		AttributeComponents[i]->SetVisibility(true);
		AttributeComponents[i]->SetWorldScale3D(FVector(0.01f, 0.001f + 0.015f * Attributes[i].Value, 0.5f));
		AttributeComponents[i]->SetRelativeLocation(FVector(0.0f, 0.75f * Attributes[i].Value - 50.0f, (-15.0f) * (i + 1)));
		
		AttributeTexts[i]->SetVisibility(true);
		AttributeTexts[i]->SetText(FText::FromString(Attributes[i].Name));
		
		FLinearColor Color;
		if (InteractCapability->CanPawnInteract)
		{
			if (InteractCapability->SkillsMissing.Contains(Attributes[i].Name))
			{
				Color = Red;
			}
			else
			{
				Color = Green;
			}
		}
		else
		{
			Color = Orange;
		}
		TextMaterialDynamic = UMaterialInstanceDynamic::Create(TextMaterial, AttributeTexts[i]);
		TextMaterialDynamic->SetScalarParameterValue(FName("Intensity"), Owner->GetInteractiveName().Equals("Ghost") ? ghostIntensity : zombieIntensity);
		TextMaterialDynamic->SetVectorParameterValue(FName("EmissiveColor"), Color);
		AttributeTexts[i]->SetMaterial(0, TextMaterialDynamic);
	}

	FLinearColor Color;
	if (InteractCapability->IsReachable)
	{
		if (InteractCapability->CanPawnInteract)
		{
			if (InteractCapability->CanObjectInteract && (InteractCapability->SkillsMissing.Num() == 0))
			{
				Color = Green;
			}
			else
			{
				Color = Red;
			}
		}
		else
		{
			Color = Orange;
		}
	}
	else
	{
		Color = Blue;
	}
	TextInfo->SetText(FText::FromString(TracedActor->GetNameToDisplay()));
	TextInfo->SetVisibility(true);
	TextMaterialDynamic = UMaterialInstanceDynamic::Create(TextMaterial, TextInfo);
	TextMaterialDynamic->SetVectorParameterValue(FName("EmissiveColor"), Color);
	TextMaterialDynamic->SetScalarParameterValue(FName("Intensity"), Owner->GetInteractiveName().Equals("Ghost") ? ghostIntensity : zombieIntensity);
	TextInfo->SetMaterial(0, TextMaterialDynamic);

	TextInfo->SetMaterial(0, TextMaterialDynamic);
	RootComponent->SetWorldLocation(TracedActor->GetDisplayLocation() + FVector(0.0f, 0.0f, Attributes.Num() * 15.0f));
}

// Hide info about an object
void UObjectInfoComponent::Hide()
{
	TextInfo->SetVisibility(false);
	for (int i = 0; i < MAX_ATTRIBUTES; ++i)
	{
		AttributeComponents[i]->SetVisibility(false);
		AttributeTexts[i]->SetVisibility(false);
	}
}

void UObjectInfoComponent::SetCamera(UCameraComponent* NewCamera)
{
	Camera = NewCamera;
}