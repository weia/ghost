// Author: Anna Kobak

#include "GhostGame.h"
#include "TracingComponent.h"

// Sets default values for this component's properties
UTracingComponent::UTracingComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	bWantsBeginPlay = true;
	PrimaryComponentTick.bCanEverTick = true;
	Owner = GetOwner();
	OwnerInterface = Cast<IPawnInterface>(Owner);

	TracerMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	static ConstructorHelpers::FObjectFinder<UStaticMesh> StaticMesh(TEXT("StaticMesh'/Game/StarterContent/Shapes/Shape_Sphere.Shape_Sphere'"));
	static ConstructorHelpers::FObjectFinder<UMaterial> Material(TEXT("Material'/Game/StarterContent/Materials/M_TraceDot.M_TraceDot'"));

	TracerMeshComponent->SetCollisionResponseToAllChannels(ECR_Ignore);
	TracerMeshComponent->SetWorldScale3D(FVector(0.05f, 0.05f, 0.05f));
	TracerMeshComponent->SetStaticMesh(StaticMesh.Object);
	TracerMeshComponent->SetMaterial(0, Material.Object);
	TracerMeshComponent->SetVisibility(false);
}


// Called when the game starts
void UTracingComponent::BeginPlay()
{
	TracerMeshComponent->SetWorldLocation(Owner->GetActorLocation());
	Super::BeginPlay();

	bStareToInteract = Cast<AMenuCharacter>(Owner) != nullptr;
}


// Called every frame
void UTracingComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	if (!bIsTracerActive)
		return;
	//Check if camera is attached in the same place as our component
	APlayerCameraManager* Manager = GetWorld()->GetFirstPlayerController()->PlayerCameraManager;
	//Calculate Start and End
	FVector Start, End;
	Start = Manager->GetCameraLocation();
	End = Start + Manager->GetActorForwardVector()*TracingLength;

	//Prepare Line Trace
	const FName TraceTag("MyTraceTag");
	//World->DebugDrawTraceTag = TraceTag;
	FCollisionQueryParams TraceParams = FCollisionQueryParams(FName(TEXT("RV_Trace")), true, Owner);
	TraceParams.bTraceComplex = true;
	TraceParams.bTraceAsyncScene = true;
	//TraceParams.bReturnPhysicalMaterial = false;
	//TraceParams.TraceTag = TraceTag;
	FHitResult Hit(ForceInit);

    //Line Trace 
	GetWorld()->LineTraceSingleByChannel(Hit, Start, End, ECC_Visibility, TraceParams);

	FVector TraceDotLocation = Start + Manager->GetActorForwardVector()*200.0f;
	TracerMeshComponent->SetWorldLocation(TraceDotLocation, true);
	AActor* NewTracedActor = Hit.GetActor();
	TracedComponent = Hit.GetComponent();
	HitLocation = Hit.Location;

	if (bStareToInteract)
	{
		//If we traced the same actor as before and it's menu opiton, just increase interaction progress
		if (NewTracedActor == TracedActor)
		{
			if (bIsInteractEnabled)
			{
				stareInteractionProgress -= 0.025f * DeltaTime;
				if (stareInteractionProgress < 0.0f)
				{
					stareInteractionProgress = 0.0f;
					Cast<AGhostCharacter>(Owner)->DeactivateTracingComponent();
					HandleInteractEvent();
				}
				TracerMeshComponent->SetWorldScale3D(FVector(stareInteractionProgress, stareInteractionProgress, stareInteractionProgress));
			}
			return;
		}

		//Different component

		//Reset interaction progress
		stareInteractionProgress = 0.05f;
		TracedActor = NewTracedActor;

		//Handle NULL actor
		if (TracedActor == NULL) {
			TracedActorInteractive = nullptr;
			TracedActorDisplayable = nullptr;
			bIsInteractEnabled = false;
			return;
		}

		//Handle not NULL actor
		TracedActorInteractive = Cast<IInteractive>(TracedActor);
		TracedActorDisplayable = Cast<IDisplayable>(TracedActor);
		bIsInteractEnabled = Cast<AMenuOption>(Hit.GetActor()) != nullptr;
		TracerMeshComponent->SetWorldScale3D(FVector(stareInteractionProgress, stareInteractionProgress, stareInteractionProgress));
	}
	else
	{
		//If we traced the same actor as before, we only need to update the message
		if (NewTracedActor == TracedActor) {
			bIsInteractEnabled = IsInteractEnabled();
			StartDisplayingMessage();
			return;
		}

		//Different component

		//Finish with previous component
		StopDisplayingMessage();

		//And prepare the next
		TracedActor = NewTracedActor;

		//Handle NULL actor
		if (TracedActor == NULL) {
			TracedActorInteractive = nullptr;
			TracedActorDisplayable = nullptr;
			bIsInteractEnabled = false;
			return;
		}

		//Handle not NULL actor
		TracedActorInteractive = Cast<IInteractive>(TracedActor);
		TracedActorDisplayable = Cast<IDisplayable>(TracedActor);
		bIsInteractEnabled = IsInteractEnabled();
		StartDisplayingMessage();
	}
}

void UTracingComponent::StartDisplayingMessage()
{
	static FInteractCapability LastInteractCapability;

	//Nothing changed, don't do anything
	if (bIsMessageDisplayed && LastInteractCapability == InteractCapability)
		return;

	StopDisplayingMessage();
	bIsMessageDisplayed = false;
	if (TracedActorDisplayable != nullptr) {
		OwnerInterface->DisplayObjectInfo(TracedActorDisplayable, &InteractCapability);
		bIsMessageDisplayed = true;
	}

	LastInteractCapability = InteractCapability;
}


void UTracingComponent::StopDisplayingMessage()
{
	if(bIsMessageDisplayed) {}
		OwnerInterface->HideObjectInfo();
	bIsMessageDisplayed = false;
}


bool UTracingComponent::IsInteractEnabled()
{
	InteractCapability.Reset();
	if (TracedActorInteractive == nullptr)
		return false;
	bool bObjectEnabled = TracedActorInteractive->IsInteractEnabled(Owner, &InteractCapability);
	bool bPawnEnabled = OwnerInterface->IsInteractWithEnabled(TracedActor, &InteractCapability);

	float DistanceToHit = (TracedActor->GetActorLocation() - Owner->GetActorLocation()).Size();
	InteractCapability.IsReachable = DistanceToHit < InteractDistance;

	return bObjectEnabled && bPawnEnabled && DistanceToHit < InteractDistance;
}

void UTracingComponent::HandleInteractEvent()
{
	if (bIsInteractEnabled) {
		OwnerInterface->InteractWith(TracedActor);
		TracedActorInteractive->Interact(Owner);
	}
}

void UTracingComponent::ActivateTracer()
{
	bIsTracerActive = true;
	TracerMeshComponent->SetVisibility(bIsTraceDotVisible);
}

void UTracingComponent::DeactivateTracer()
{
	StopDisplayingMessage();
	bIsTracerActive = false;
	TracerMeshComponent->SetVisibility(false);

}

void UTracingComponent::SetTraceDotVisibility(bool bIsVisible)
{
	TracerMeshComponent->SetVisibility(bIsVisible);
	bIsTraceDotVisible = bIsVisible;

}

bool UTracingComponent::IsTraceDotVisible()
{
	return bIsTraceDotVisible;
}

bool UTracingComponent::bIsTraceDotVisible = true;

AActor* UTracingComponent::GetTracedActor()
{
	return TracedActor;
}

UPrimitiveComponent* UTracingComponent::GetTracedComponent()
{
	return TracedComponent;
}

FVector UTracingComponent::GetHitLocation()
{
	return HitLocation;
}
