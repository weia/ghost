// Component responsible for displaying information about an object that a pawn looks at

#pragma once

#include "Components/ActorComponent.h"
#include "../Interfaces/Displayable.h"
#include "../Interfaces/PawnInterface.h"
#include "ObjectInfoComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class GHOSTGAME_API UObjectInfoComponent : public USceneComponent
{
	GENERATED_BODY()

private:
	static const int MAX_ATTRIBUTES = 9;

	IPawnInterface* Owner;
	USceneComponent* RootComponent;
	IDisplayable* TracedObject;
	// Active camera of the owner
	UCameraComponent* Camera;
	// Info to display
	UTextRenderComponent* TextInfo;
	UStaticMeshComponent* AttributeComponents[MAX_ATTRIBUTES];
	UTextRenderComponent* AttributeTexts[MAX_ATTRIBUTES];
	UMaterialInterface* TextMaterial;
	UMaterialInstanceDynamic* TextMaterialDynamic;
	// Display constants
	const FLinearColor Green = FLinearColor(0.0f, 0.2f, 0.0f, 0.0f);
	const FLinearColor Red = FLinearColor(0.2f, 0.0f, 0.0f, 0.0f);
	const FLinearColor Orange = FLinearColor(0.3f, 0.05f, 0.0f, 0.0f);
	const FLinearColor Blue = FLinearColor(0.0f, 0.07f, 0.18f, 0.0f);
	const float ghostIntensity = 40.0f;
	const float zombieIntensity = 2.0f;

public:
	// Sets default values for this component's properties
	UObjectInfoComponent();

	// Called when the game starts
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;

	// Display info about a given object
	void Display(IDisplayable* TracedActor, FInteractCapability* InteractCapability);
	void Hide();

	void SetCamera(UCameraComponent* NewCamera);
};
