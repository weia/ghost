// Author: Grzegorz Bukowiec
// A TextRender component that can display a given text message on screen,
// meant to be attached to a pawn's camera

#include "GhostGame.h"
#include "../Interfaces/PawnInterface.h"
#include "PlayerInfoComponent.h"
#include <vector>


// Sets default values for this component's properties
UPlayerInfoComponent::UPlayerInfoComponent()
{
	// Set this component to be initialized when the game starts, and not to be ticked every frame
	bWantsBeginPlay = true;
	PrimaryComponentTick.bCanEverTick = false;

	// Create and set spring arm
	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	SpringArm->TargetArmLength = -1.0f;
	SpringArm->bUsePawnControlRotation = true;
	SpringArm->bEnableCameraRotationLag = true;
	SpringArm->CameraRotationLagSpeed = 10;

	// Create TextRenderComponent and set its properties
	TextInfo = CreateDefaultSubobject<UTextRenderComponent>(TEXT("TextInfo"));
	TextInfo->AttachTo(SpringArm);
	TextInfo->SetText(FText::FromString("Info"));
	TextInfo->SetWorldSize(32.0f);
	TextInfo->SetWorldLocation(FVector(400.0f, 0.0f, 0.0f));
	TextInfo->SetWorldRotation(FRotator(0.0f, 180.0f, 0.0f));
	TextInfo->VerticalAlignment = EVerticalTextAligment::EVRTA_TextCenter;
	TextInfo->HorizontalAlignment = EHorizTextAligment::EHTA_Center;
	TextInfo->SetVisibility(false);

	// Set text material
	static ConstructorHelpers::FObjectFinder<UMaterialInterface> MaterialAsset(TEXT("/Game/Materials/EmissiveText/M_EmissiveText"));
	TextMaterial = MaterialAsset.Object;
}


// Called when the game starts
void UPlayerInfoComponent::BeginPlay()
{
	Super::BeginPlay();
	UMaterialInstanceDynamic* DynamicMaterial = UMaterialInstanceDynamic::Create(TextMaterial, TextInfo);
	DynamicMaterial->SetScalarParameterValue(FName("Intensity"), Cast<IPawnInterface>(GetOwner())->GetInteractiveName().Equals("Ghost") ? ghostIntensity : zombieIntensity);
	TextInfo->SetMaterial(0, DynamicMaterial);
}


// Called every frame
void UPlayerInfoComponent::TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction )
{
	Super::TickComponent( DeltaTime, TickType, ThisTickFunction );
}

// Displays given text on screen
void UPlayerInfoComponent::Show(FString NewTextInfo)
{
	TextInfo->SetText(Split(NewTextInfo));
	TextInfo->SetVisibility(true);
}

// Hides previously displayed text
void UPlayerInfoComponent::Hide()
{
	TextInfo->SetVisibility(false);
}

// Attaches TextInfo to the given outer component
void UPlayerInfoComponent::AttachTo(USceneComponent* Parent)
{
	SpringArm->AttachTo(Parent);
}

// Divides a long string into several lines
FText UPlayerInfoComponent::Split(FString RawText)
{
	// Divide string intro words
	FString Left = "", Right = RawText, Result = "";
	std::vector<FString> Words;
	int length = 0;
	while (true)
	{
		if (!Right.Split(" ", &Left, &Right))
		{
			Words.push_back(Right);
			break;
		}
		Words.push_back(Left);
	}

	// Build lines of limited length
	for (int i = 0; i < Words.size(); ++i)
	{
		if (length + Words[i].Len() > 20 && length > 0)
		{
			Result += "\n";
			length = 0;
		}
		else if (length > 0)
		{
			Result += " ";
			length++;
		}
		Result += Words[i];
		length += Words[i].Len();
	}

	return FText::FromString(Result);
}