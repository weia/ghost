// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/SceneComponent.h"
#include "../Interfaces/Interactive.h"
#include "../Interfaces/Displayable.h"
#include "../Interfaces/PawnInterface.h"
#include "../Utilities/InteractCapability.h"
#include "../InteractiveObjects/MenuOption.h"
#include "TracingComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class GHOSTGAME_API UTracingComponent : public USceneComponent
{
	GENERATED_BODY()

	UStaticMeshComponent *TracerMeshComponent;


private:
	AActor* Owner;
	IPawnInterface* OwnerInterface;
	const int TracingLength = 10000;
	const int InteractDistance = 300;
	FVector HitLocation;
	UPrimitiveComponent* TracedComponent;
	AActor* TracedActor;
	IInteractive* TracedActorInteractive;
	IDisplayable* TracedActorDisplayable;
	bool bIsInteractEnabled = false;
	FInteractCapability InteractCapability;
	FString PawnMessage;
	FString ObjectMessage;
	FString DisplayMessage;
	bool bIsTracerActive = false;
	bool bIsMessageDisplayed = false;
	bool IsInteractEnabled();
	bool bStareToInteract = false;
	float stareInteractionProgress = 0.05f;
	static bool bIsTraceDotVisible;
	void StartDisplayingMessage();
	void StopDisplayingMessage();
public:	
	UTracingComponent();

	// Called when the game starts
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;
	
	//Calls interact in player and object
	//Only if both player's and object's interactions are enabled
	void HandleInteractEvent();

	void DeactivateTracer();
	void ActivateTracer();

	
	void SetTraceDotVisibility(bool bIsVisible);
	bool IsTraceDotVisible();

	AActor* GetTracedActor();
	UPrimitiveComponent* GetTracedComponent();
	FVector GetHitLocation();

};
