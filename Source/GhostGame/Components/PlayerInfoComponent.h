// A TextRender component that can display a given text message on screen,
// meant to be attached to a pawn's camera

#pragma once

#include "Components/ActorComponent.h"
#include "PlayerInfoComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class GHOSTGAME_API UPlayerInfoComponent : public USceneComponent
{
	GENERATED_BODY()

private:
	// Divides a long string into several lines
	FText Split(FString RawText);

	// Text to display on screen
	UTextRenderComponent* TextInfo;
	// Enables camera lag
	USpringArmComponent* SpringArm;

	//Display constants
	const float ghostIntensity = 100.0f;
	const float zombieIntensity = 5.0f;

	UMaterialInterface* TextMaterial;

public:	
	// Sets default values for this component's properties
	UPlayerInfoComponent();
	// Called when the game starts
	virtual void BeginPlay() override;	
	// Called every frame
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;
	// Displays given text on screen
	void Show(FString NewTextInfo);
	// Hides previously displayed text
	void Hide();
	// Attaches TextInfo to the given outer component
	void AttachTo(USceneComponent* Parent);
};
