// Author: Anna Kobak

#include "GhostGame.h"
#include "FunctionalObjectsArray.h"

FFunctionalObjectsArray::FFunctionalObjectsArray()
{
}

FFunctionalObjectsArray::~FFunctionalObjectsArray()
{
}

void FFunctionalObjectsArray::PerformForEachFunctional(std::function<void(IFunctional*, FFunctionalPair*)> func)
{
	for (int i = 0; i < Objects->Num(); i++)
		if ((*Objects)[i].Actor != NULL) {
			IFunctional* Object = Cast<IFunctional>((*Objects)[i].Actor);
			bool bSync = (*Objects)[i].bIsAnimationSynchronized;
			if (Object != nullptr)
				func(Object, &(*Objects)[i]);
		}
}

void FFunctionalObjectsArray::CallActivateOrDeactivate(IFunctional* Object, FFunctionalPair* Pair, FString Action, AActor* Caller, bool bShowAnimation)
{
	IInteractive* ICaller;
	if (Caller != nullptr)
		ICaller = Cast<IInteractive>(Caller);
	else
		ICaller = nullptr;
	if (Action == "Activate" ^ !Pair->bActivateOnActivation) {
		if (!bShowAnimation) {
			Object->ActivateFunctionality(false, false);
				return;
		}
		Pair->bAgreedToBeginAnimation = false;
		if (Object->CanActivate()) {
			Object->ActivateFunctionality(Pair->bIsAnimationSynchronized);
			if (Pair->bIsAnimationSynchronized) {
				Object->BeginOfAnimation(ICaller);
				Pair->bAgreedToBeginAnimation = true;
			}
		}
	} else {
		if (!bShowAnimation) {
			Object->DeactivateFunctionality(false, false);
			return;
		}
		Pair->bAgreedToBeginAnimation = false;
		if (Object->CanDeactivate()) {
			Object->DeactivateFunctionality(Pair->bIsAnimationSynchronized);
			if (Pair->bIsAnimationSynchronized) {
				Object->BeginOfAnimation(ICaller);
				Pair->bAgreedToBeginAnimation = true;
			}
		}
	}
}

void FFunctionalObjectsArray::CallActivateFunctionalityInAll(AActor* Caller, bool bShowAnimation)
{
	PerformForEachFunctional([=](IFunctional* Object, FFunctionalPair* Pair) { 
		CallActivateOrDeactivate(Object, Pair, "Activate", Caller, bShowAnimation);
	});
}

void FFunctionalObjectsArray::CallDeactivateFunctionalityInAll(AActor* Caller, bool bShowAnimation)
{
	PerformForEachFunctional([=](IFunctional* Object, FFunctionalPair* Pair) {
		CallActivateOrDeactivate(Object, Pair, "Deactivate", Caller, bShowAnimation);
	});
}

void FFunctionalObjectsArray::CallScheduleActivationInAll()
{
	PerformForEachFunctional([](IFunctional* Object, FFunctionalPair* Pair) {
		if (Pair->bActivateOnActivation)
			Object->ScheduleActivation();
		else
			Object->ScheduleDeactivation();
	});
}

void FFunctionalObjectsArray::CallScheduleDeactivationInAll()
{
	PerformForEachFunctional([](IFunctional* Object, FFunctionalPair* Pair) {
		if (Pair->bActivateOnActivation)
			Object->ScheduleDeactivation();	
		else
			Object->ScheduleActivation();
	});
}



void FFunctionalObjectsArray::CallAnimationTimelineFloatReturnInAll(float val)
{
	PerformForEachFunctional([val](IFunctional* Object, FFunctionalPair* Pair) {
		if(Pair->bAgreedToBeginAnimation) 
			Object->AnimationTimelineFloatReturn(val);
	});
}

void FFunctionalObjectsArray::CallEndOfAnimationInAll()
{
	PerformForEachFunctional([](IFunctional* Object, FFunctionalPair* Pair) {
		if (Pair->bAgreedToBeginAnimation)
			Object->EndOfAnimation();
	});
}



void FFunctionalObjectsArray::SetArray(TArray<FFunctionalPair>* NewObjects)
{
	if(NewObjects != nullptr)
		Objects = NewObjects;
}

void FFunctionalObjectsArray::PerformBeginPlayActivation(bool bIsActivated)
{
	PerformForEachFunctional([bIsActivated](IFunctional* Object, FFunctionalPair* Pair) {
		if (Pair->bActivateOnActivation ^ !bIsActivated)
			Object->ActivateFunctionality(false, false);
	});
}
