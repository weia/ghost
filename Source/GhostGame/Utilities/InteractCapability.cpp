// Author: Grzegorz Bukowiec

#include "GhostGame.h"
#include "InteractCapability.h"

FInteractCapability::FInteractCapability()
{
	IsReachable = true;
	CanPawnInteract = true;
	CanObjectInteract = true;
}

FInteractCapability::~FInteractCapability()
{
}

FInteractCapability::FInteractCapability(bool NewIsReachable, bool NewCanPawnInteract, bool NewCanObjectInteract, TArray<FString>  NewSkillsMissing)
{
	IsReachable = NewIsReachable;
	CanPawnInteract = NewCanPawnInteract;
	CanObjectInteract = NewCanObjectInteract;
	SkillsMissing = NewSkillsMissing;
}

void FInteractCapability::Reset()
{
	IsReachable = true;
	CanPawnInteract = true;
	CanObjectInteract = true;
	SkillsMissing.Empty();
}

bool FInteractCapability::operator==(const FInteractCapability& X)
{
	if (IsReachable != X.IsReachable)
	{
		return false;
	}
	if (CanPawnInteract != X.CanPawnInteract)
	{
		return false;
	}
	if (CanObjectInteract != X.CanObjectInteract)
	{
		return false;
	}
	if (SkillsMissing.Num() != X.SkillsMissing.Num())
	{
		return false;
	}
	for (FString skill : SkillsMissing)
	{
		if (X.SkillsMissing.Find(skill) == false)
		{
			return false;
		}
	}

	return true;
}