// Fill out your copyright notice in the Description page of Project Settings.

#include "GhostGame.h"
#include "PawnAttribute.h"
#include "InteractCapability.generated.h"

#pragma once


USTRUCT()
struct FInteractCapability {
	GENERATED_USTRUCT_BODY()
	UPROPERTY(EditAnywhere)
		bool IsReachable;
	UPROPERTY(EditAnywhere)
		bool CanPawnInteract;
	UPROPERTY(EditAnywhere)
		bool CanObjectInteract;
	UPROPERTY(EditAnywhere)
		TArray<FString>  SkillsMissing;

	FInteractCapability();
	~FInteractCapability();
	FInteractCapability(bool IsReachable, bool CanPawnInteract, bool CanObjectInteract, TArray<FString> SkillsMissing);
	void Reset();
	bool operator==(const FInteractCapability& X);
};


