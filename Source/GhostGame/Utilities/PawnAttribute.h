// Fill out your copyright notice in the Description page of Project Settings.

#include "GhostGame.h"
#include "PawnAttribute.generated.h"

#pragma once

USTRUCT()
struct FPawnAttribute {
	GENERATED_USTRUCT_BODY()
		UPROPERTY(EditAnywhere)
		FString Name;
	UPROPERTY(EditAnywhere)
		int32 Value;

	FPawnAttribute();
	~FPawnAttribute();
	FPawnAttribute(FString NewName, int32 NewValue); 
};