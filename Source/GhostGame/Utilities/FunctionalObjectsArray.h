// Fill out your copyright notice in the Description page of Project Settings.

#include "GhostGame.h"
#include <functional>

#include "../Interfaces/Functional.h"
#include "FunctionalObjectsArray.generated.h"


#pragma once

USTRUCT()
struct FFunctionalPair {
	GENERATED_USTRUCT_BODY()
	UPROPERTY(EditAnywhere)
		AActor* Actor;
	UPROPERTY(EditAnywhere)
		bool bIsAnimationSynchronized = false;
	//true - on Object Activation call ActivateFunctionality
	//false - on Object Activation call DeactivateFunctionality
	UPROPERTY(EditAnywhere)
		bool bActivateOnActivation = true;
	bool bAgreedToBeginAnimation = false;
};


USTRUCT()
struct FFunctionalObjectsArray {
	GENERATED_USTRUCT_BODY()

	TArray<FFunctionalPair>* Objects;
	FFunctionalObjectsArray();
	~FFunctionalObjectsArray();

	void CallActivateFunctionalityInAll(AActor* Caller = nullptr, bool bShowAnimation = true);
	void CallDeactivateFunctionalityInAll(AActor* Caller = nullptr, bool bShowAnimation = true);
	void CallScheduleActivationInAll();
	void CallScheduleDeactivationInAll();
	void CallAnimationTimelineFloatReturnInAll(float val);
	void CallEndOfAnimationInAll();
	void SetArray(TArray<FFunctionalPair>* NewObjects);
	void PerformBeginPlayActivation(bool bIsActivated);

	private:
	void CallActivateOrDeactivate(IFunctional* Object, FFunctionalPair* Pair, FString Action, AActor* Caller, bool bShowAnimation);
	void PerformForEachFunctional(std::function<void(IFunctional*, FFunctionalPair*)> func);
};