// Author: Anna Kobak

#include "GhostGame.h"
#include "PawnAttribute.h"

FPawnAttribute::FPawnAttribute()
{
}

FPawnAttribute::~FPawnAttribute()
{

}

FPawnAttribute::FPawnAttribute(FString NewName, int32 NewValue)
{
	Name = NewName;
	Value = NewValue;
}
