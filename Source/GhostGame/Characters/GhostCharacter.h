// A pawn that is able to fly and pass through some specific object

#pragma once

#include "GameFramework/Pawn.h"
#include "../Interfaces/PawnInterface.h"
#include "../Components/PlayerInfoComponent.h"
#include "../Components/TracingComponent.h"
#include "../Components/ObjectInfoComponent.h"
#include "../Interfaces/Displayable.h"
#include "ZombieCharacter.h"
#include "GhostCharacter.generated.h"

UCLASS()
class GHOSTGAME_API AGhostCharacter : public APawn, public IPawnInterface
{
	GENERATED_BODY()

private:
	// Moving
	void MoveForward(float AxisValue);
	void MoveRight(float AxisValue);
	// Looking around
	void TurnRight(float AxisValue);
	void TurnUp(float AxisValue);
	// Flying
	void RiseUp();
	void Fall();

	void ToggleVRMode();
	void InteractAction();
	void ToggleTraceDotVisibility();
	virtual void ResumeGame();
	void ExitGame();
	void WaitForInput();


	UStaticMeshComponent* GhostVision;
	UPointLightComponent* Light;
	UCameraComponent* Camera;
	class UGhostMovement* GhostMovementComponent;
	class UPlayerInfoComponent* TextOnScreenComponent;
	class UObjectInfoComponent* ObjectInfoComponent;
	class UTracingComponent* TracingComponent;

	UMaterialInterface* GhostVisionInstance;
	

	//Timeline related variables
	UPROPERTY()
		UTimelineComponent* ScreenFadeTimeline;
	UPROPERTY()
		UCurveFloat* ScreenFadeCurve;
	FOnTimelineFloat InterpFunction{};
	FOnTimelineEvent TimelineFinishedEvent;
	IInteractive* AnimationCallerActor;

	float currentVelocityUp = -0.25f;
	bool bWaitForInput = true;
	bool bFadeIn = true;
	static bool VRMode;

public:
	// Sets default values for this pawn's properties
	AGhostCharacter();
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	// Called every frame
	virtual void Tick(float DeltaSeconds) override;
	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	// Toggle speed
	void IncreaseSpeed();
	void DecreaseSpeed();

	// IPawnInterface
	virtual void DisplayInfo(FString TextInfo) override;
	virtual void DisplayObjectInfo(IDisplayable* TracedActor, FInteractCapability* InteractCapability) override;
	virtual void HideObjectInfo() override;
	virtual void InteractWith(AActor* actor) override;
	virtual FString GetInteractiveName() override;

	void PrepareToBePossessed();

	// Overriden function returning movement component
	virtual UPawnMovementComponent* GetMovementComponent() const override;

	void DeactivateTracingComponent();

	//Screen fading
	void FadeIn();
	void FadeOut();
	void FadeOutLate();

	//Screen fading timeline related
	UFUNCTION()
		void ScreenFadeTimelineFloatReturn(float val);
	UFUNCTION()
		void TimelineFinishedFunction();
};