// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Pawn.h"
#include "GhostCharacter.h"
#include "MenuCharacter.generated.h"

UCLASS()
class GHOSTGAME_API AMenuCharacter : public AGhostCharacter
{
	GENERATED_BODY()

private:
	virtual void ResumeGame() override;

public:
	// Sets default values for this pawn's properties
	AMenuCharacter();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
	
};
