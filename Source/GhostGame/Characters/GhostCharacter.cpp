// Author: Grzegorz Bukowiec

#include "GhostGame.h"
#include "GhostCharacter.h"
#include "PawnUtilities/GhostMovement.h"

bool AGhostCharacter::VRMode = false;

// Sets default values
AGhostCharacter::AGhostCharacter()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	AutoPossessPlayer = EAutoReceiveInput::Player0;
	SetActorEnableCollision(true);
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = true;
	bUseControllerRotationRoll = false;

	// Collision sphere
	USphereComponent* SphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("CollisionSphere"));
	RootComponent = SphereComponent;
	SphereComponent->InitSphereRadius(80);
	SphereComponent->SetCollisionProfileName(TEXT("ImmaterialPawn"));

	// "Ghost vision" inward cube
	GhostVision = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("GhostVision"));
	//GhostVision->AttachTo(RootComponent);
	static ConstructorHelpers::FObjectFinder<UStaticMesh> GhostVisionAsset(TEXT("/Game/Meshes/InwardSphere"));
	GhostVision->SetStaticMesh(GhostVisionAsset.Object);
	GhostVision->SetWorldScale3D(FVector(0.8f));

	// Set material of a ghost vision
	static ConstructorHelpers::FObjectFinder<UMaterialInterface> MaterialAsset(TEXT("/Game/Materials/M_GhostVision"));
	GhostVisionInstance = MaterialAsset.Object;
	class UMaterialInstanceDynamic* DynamicMaterial = UMaterialInstanceDynamic::Create(GhostVisionInstance, GhostVision);
	GhostVision->SetMaterial(0, DynamicMaterial);

	// Camera
	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	Camera->AttachTo(RootComponent);
	Camera->bUsePawnControlRotation = true;

	// Light
	Light = CreateDefaultSubobject<UPointLightComponent>(TEXT("Light"));
	Light->SetIntensity(1000000.0f);
	Light->SetAttenuationRadius(100000.0f);
	Light->SetLightColor(FLinearColor(FColor(0x57, 0x3C, 0x2F)));
	Light->SetSourceRadius(80.f);
	Light->AttachTo(RootComponent);

	// Custom movement component
	GhostMovementComponent = CreateDefaultSubobject<UGhostMovement>(TEXT("GhostMovementComponent"));
	GhostMovementComponent->UpdatedComponent = RootComponent;

	// Component for diplaying text messages
	TextOnScreenComponent = CreateDefaultSubobject<UPlayerInfoComponent>(TEXT("TextOnScreen"));
	TextOnScreenComponent->AttachTo(Camera);

	// Component for diplaying info of traced objects
	ObjectInfoComponent = CreateDefaultSubobject<UObjectInfoComponent>(TEXT("ObjectInfo"));
	ObjectInfoComponent->SetCamera(Camera);

	// Component for tracing and interaction
	TracingComponent = CreateDefaultSubobject<UTracingComponent>(TEXT("TracingComponent"));
	TracingComponent->AttachTo(Camera);
	TracingComponent->ActivateTracer();

	//Timeline
	ConstructorHelpers::FObjectFinder<UCurveFloat> Curve(TEXT("CurveFloat'/Game/Blueprints/Timelines/ScreenFadeCurveBP.ScreenFadeCurveBP'"));
	ScreenFadeCurve = Curve.Object;
	ScreenFadeTimeline = CreateDefaultSubobject<UTimelineComponent>(TEXT("ScreenFadeTimeline"));
	ScreenFadeTimeline->SetTimelineLength(1.0f);
	InterpFunction.BindUFunction(this, FName{ TEXT("ScreenFadeTimelineFloatReturn") });
	TimelineFinishedEvent.BindUFunction(this, FName{ TEXT("TimelineFinishedFunction") });
}

// Called when the game starts or when spawned
void AGhostCharacter::BeginPlay()
{
	Super::BeginPlay();

	if (VRMode)
	{
		Cast<UGhostMovement>(GetMovementComponent())->ToggleVRMode();
	}
	//Move a little bit on start, to activate first TriggeredInfo
	GhostMovementComponent->AddInputVector(GetActorForwardVector().ComponentMin(FVector(1.0f, 1.0f, 0.0f) * 0.001));
	GetWorld()->GetFirstPlayerController()->PlayerCameraManager->bFollowHmdOrientation = UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled();

	//Timeline
	ScreenFadeTimeline->SetTimelineFinishedFunc(TimelineFinishedEvent);
	ScreenFadeTimeline->AddInterpFloat(ScreenFadeCurve, InterpFunction, FName{ TEXT("InterpFloat") });
}

// Called every frame
void AGhostCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// Fly or fall every frame
	GhostMovementComponent->AddInputVector(FVector(0.0f, 0.0f, 1.0f) * currentVelocityUp);

	//Stick to the HMD camera
	GhostVision->SetWorldLocation(GetWorld()->GetFirstPlayerController()->PlayerCameraManager->GetCameraLocation());
}

// Called to bind functionality to input
void AGhostCharacter::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

	// Bind action mappings
	InputComponent->BindAction("MoveUp", IE_Pressed, this, &AGhostCharacter::RiseUp);
	InputComponent->BindAction("MoveUp", IE_Released, this, &AGhostCharacter::Fall);
	InputComponent->BindAction("PrimaryAction", IE_Pressed, this, &AGhostCharacter::InteractAction);
	InputComponent->BindAction("SecondaryAction", IE_Pressed, this, &AGhostCharacter::InteractAction);
	InputComponent->BindAction("SwitchCamera", IE_Pressed, this, &AGhostCharacter::ToggleVRMode);
	InputComponent->BindAction("ToggleTraceDotVisibility", IE_Pressed, this, &AGhostCharacter::ToggleTraceDotVisibility);
	InputComponent->BindAction("ToggleRun", IE_Pressed, this, &AGhostCharacter::IncreaseSpeed);
	InputComponent->BindAction("ToggleRun", IE_Released, this, &AGhostCharacter::DecreaseSpeed);
	InputComponent->BindAction("ResumeGame", IE_Pressed, this, &AGhostCharacter::ResumeGame);
	InputComponent->BindAction("ExitGame", IE_Pressed, this, &AGhostCharacter::ExitGame);

	// Bind axis mappings
	InputComponent->BindAxis("MoveForward", this, &AGhostCharacter::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &AGhostCharacter::MoveRight);
	InputComponent->BindAxis("TurnRight", this, &AGhostCharacter::TurnRight);
	InputComponent->BindAxis("TurnUp", this, &AGhostCharacter::TurnUp);
}

// Moving
void AGhostCharacter::MoveForward(float AxisValue)
{
	GhostMovementComponent->AddInputVector(GWorld->GetFirstPlayerController()->PlayerCameraManager->GetActorForwardVector().ComponentMin(FVector(1.0f, 1.0f, 0.0f)).ComponentMax(FVector(-1.0f, -1.0f, 0.0f)) * AxisValue);
}
void AGhostCharacter::MoveRight(float AxisValue)
{
	GhostMovementComponent->AddInputVector(GWorld->GetFirstPlayerController()->PlayerCameraManager->GetActorRightVector().ComponentMin(FVector(1.0f, 1.0f, 0.0f)) * AxisValue);
}

// Looking around
void AGhostCharacter::TurnRight(float AxisValue)
{
	AddControllerYawInput(AxisValue);
}
void AGhostCharacter::TurnUp(float AxisValue)
{
	if (!UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled()) {
		if (GetControlRotation().Pitch + (GetWorld()->GetFirstPlayerController()->InputPitchScale) * AxisValue < 80.0 || GetControlRotation().Pitch + (-1.75f) * AxisValue > 280.f)
		{
			AddControllerPitchInput(AxisValue);
		}
	}
}

// Flying
void AGhostCharacter::RiseUp()
{
	currentVelocityUp = 0.5f;
}
void AGhostCharacter::Fall()
{
	currentVelocityUp = -0.25f;
}

// Toggle speed
void AGhostCharacter::IncreaseSpeed()
{
	Cast<UGhostMovement>(GetMovementComponent())->SetSpeedBoost(true);
}
void AGhostCharacter::DecreaseSpeed()
{
	Cast<UGhostMovement>(GetMovementComponent())->SetSpeedBoost(false);
}

void AGhostCharacter::ToggleVRMode()
{
	Cast<UGhostMovement>(GetMovementComponent())->ToggleVRMode();
	VRMode = !VRMode;
}

//Trace Dot Visibility
void AGhostCharacter::ToggleTraceDotVisibility()
{
	TracingComponent->SetTraceDotVisibility(!TracingComponent->IsTraceDotVisible());
}

void AGhostCharacter::ResumeGame()
{
	if (bWaitForInput && !GetMovementComponent()->IsActive())
	{
		TextOnScreenComponent->Hide();
		GetMovementComponent()->Activate();
	}
}

void AGhostCharacter::WaitForInput()
{
	bWaitForInput = true;
}

void AGhostCharacter::ExitGame()
{
	GetWorld()->GetFirstPlayerController()->ConsoleCommand("quit");
}

// Overriden function returning movement component
UPawnMovementComponent* AGhostCharacter::GetMovementComponent() const
{
	return GhostMovementComponent;
}

void AGhostCharacter::InteractAction()
{
	TracingComponent->HandleInteractEvent();
}

// IPawnInterface functions
void AGhostCharacter::DisplayInfo(FString TextInfo)
{
	bWaitForInput = false;
	FTimerHandle UnusedHandle;
	GetWorld()->GetTimerManager().SetTimer(UnusedHandle, this, &AGhostCharacter::WaitForInput, 0.5f, false);
	GetMovementComponent()->Deactivate();
	GetMovementComponent()->StopMovementImmediately();
	TextOnScreenComponent->Show(TextInfo);
}

void AGhostCharacter::DisplayObjectInfo(IDisplayable* TracedActor, FInteractCapability* InteractCapability)
{
	ObjectInfoComponent->Display(TracedActor, InteractCapability);
}
void AGhostCharacter::HideObjectInfo()
{
	ObjectInfoComponent->Hide();
}

void AGhostCharacter::InteractWith(AActor* actor)
{
	if (actor != NULL && Cast<AZombieCharacter>(actor) != nullptr)
	{
		if (GhostMovementComponent->IsFast())
		{
			Cast<AZombieCharacter>(actor)->StartRunning();
		}
		else
		{
			Cast<AZombieCharacter>(actor)->StopRunning();
		}
		Destroy();
	}
}

FString AGhostCharacter::GetInteractiveName()
{
	return "Ghost";
}

// Screen fading
void AGhostCharacter::FadeIn()
{
	bFadeIn = true;
	ScreenFadeTimeline->PlayFromStart();
}

void AGhostCharacter::FadeOut()
{
	bFadeIn = false;
	ScreenFadeTimeline->PlayFromStart();
}

void AGhostCharacter::FadeOutLate()
{
	FTimerHandle UnusedHandle;
	GetWorld()->GetTimerManager().SetTimer(UnusedHandle, this, &AGhostCharacter::FadeOut, 1.0f, false);
}

void AGhostCharacter::ScreenFadeTimelineFloatReturn(float val)
{
	class UMaterialInstanceDynamic* DynamicMaterial = UMaterialInstanceDynamic::Create(GhostVisionInstance, GhostVision);
	DynamicMaterial->SetScalarParameterValue(FName("FadeOpacity"), bFadeIn ? 0.2f + 0.8f * val : 1.0 - 0.8f * val);
	GhostVision->SetMaterial(0, DynamicMaterial);
}
void AGhostCharacter::TimelineFinishedFunction()
{
	if (!bFadeIn)
	{
		class UMaterialInstanceDynamic* DynamicMaterial = UMaterialInstanceDynamic::Create(GhostVisionInstance, GhostVision);
		DynamicMaterial->SetScalarParameterValue(FName("Deviation"), 0.05f);
		DynamicMaterial->SetScalarParameterValue(FName("FadeOpacity"), 0.2f);
		GhostVision->SetMaterial(0, DynamicMaterial);
	}
}

void AGhostCharacter::DeactivateTracingComponent()
{
	TracingComponent->DeactivateTracer();
}

void AGhostCharacter::PrepareToBePossessed()
{
	//Set ghost vision material
	class UMaterialInstanceDynamic* DynamicMaterial = UMaterialInstanceDynamic::Create(GhostVisionInstance, GhostVision);
	DynamicMaterial->SetScalarParameterValue(FName("Deviation"), 0.05f);
	DynamicMaterial->SetScalarParameterValue(FName("FadeOpacity"), 0.2f);
	GhostVision->SetMaterial(0, DynamicMaterial);
}