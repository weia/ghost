// Author: Anna Kobak

#include "GhostGame.h"
#include "ZombieCharacter.h"
#include "GhostCharacter.h"

/**************************** Constructors ***********************************/

//Sets default values
AZombieCharacter::AZombieCharacter()
{
	PrimaryActorTick.bCanEverTick = true;

	// Collisions
	GetCapsuleComponent()->SetCollisionProfileName(TEXT("BlockAll"));
	GetCapsuleComponent()->SetCollisionObjectType(ECC_Pawn);
	GetCapsuleComponent()->SetCapsuleHalfHeight(100.0f);
	GetCapsuleComponent()->SetCapsuleRadius(70.0f);


	// Mesh
	GetMesh()->SetWorldRotation(FRotator(0.0f, -90.0f, 0.0f));
	GetMesh()->SetWorldLocation(FVector(0.0f, 0.0f, -95.0f));

	static ConstructorHelpers::FObjectFinder<USkeletalMesh> SkeletalMesh(TEXT("SkeletalMesh'/Game/Meshes/Mannequin/Character/Mesh/SK_Mannequin.SK_Mannequin'"));
	static ConstructorHelpers::FObjectFinder<UAnimBlueprint> AnimBlueprint(TEXT("AnimBlueprint'/Game/Meshes/Mannequin/Animations/ThirdPerson_AnimBP.ThirdPerson_AnimBP'"));
	GetMesh()->SetSkeletalMesh(SkeletalMesh.Object);
	GetMesh()->SetAnimInstanceClass(AnimBlueprint.Object->GeneratedClass);

	// First Person Camera
	FirstPersonCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCamera->AttachTo(RootComponent);
	FVector Location = FirstPersonCamera->GetComponentLocation();
	Location += FVector(40.0f, 0.00f, 90.0f);
	FirstPersonCamera->SetWorldLocation(Location);
	FirstPersonCamera->bUsePawnControlRotation = true;

	// Third Person Camera Spring Arm
	CameraSpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraSpringArm"));
	Location = CameraSpringArm->GetComponentLocation();
	CameraSpringArm->SetWorldLocation(Location + FVector(0.0f, 0.0f, 50.0f));
	FRotator Rotation = FRotator(-20.0f, 0.0f, 0.0f);
	CameraSpringArm->SetWorldRotation(Rotation);
	CameraSpringArm->TargetArmLength = InitialThirdPersonCameraOffset;
	CameraSpringArm->AttachTo(RootComponent);

	// Third Person Camera
	ThirdPersonCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("ThirdPersonCamera"));
	ThirdPersonCamera->AttachTo(CameraSpringArm);
	ThirdPersonCamera->bUsePawnControlRotation = false;

	// Detached Camera
	DetachedCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("DetachedCamera"));
	DetachedCamera->bUsePawnControlRotation = false;

	//Tracing Component
	TracingComponent = CreateDefaultSubobject<UTracingComponent>(TEXT("TracingComponent"));
	Location = FirstPersonCamera->GetComponentLocation();
	TracingComponent->SetWorldLocation(Location);
	TracingComponent->AttachTo(RootComponent);

	//Light Component
	LightComponent = CreateDefaultSubobject<UPointLightComponent>(TEXT("LightComponent"));
	LightComponent->SetIntensity(10000.0f);
	LightComponent->SetAttenuationRadius(3000.0f);
	LightComponent->SetLightColor(FLinearColor(FColor(0x57, 0x3C, 0x2F)));
	LightComponent->SetCastShadows(false);
	LightComponent->SetRelativeLocation(FVector(0.0f, 0.0f, 20.0f));
	LightComponent->AttachTo(RootComponent);

	//Player Info Component
	PlayerInfoComponent = CreateDefaultSubobject<UPlayerInfoComponent>(TEXT("PlayerInfoComponent"));

	//Object Info Component
	ObjectInfoComponent = CreateDefaultSubobject<UObjectInfoComponent>(TEXT("ObjectInfoComponent"));

	//Attributes
	PawnAttributes.Add(FPawnAttribute("Strength", 10));
	PawnAttributes.Add(FPawnAttribute("Acrobatics", 10));
	PawnAttributes.Add(FPawnAttribute("Athletics", 10));
}


/**************************** Begin Play *************************************/

// Called when the game starts or when spawned
void AZombieCharacter::BeginPlay()
{
	Super::BeginPlay();

	//Jumping
	SuperJumpVelocity = GetPawnAttributeValue("Acrobatics") * AcrobaticsScaleFactor;
	NormalJumpVelocity = FMath::Min(BasicJumpVelocity, SuperJumpVelocity);
	GetCharacterMovement()->JumpZVelocity = NormalJumpVelocity;

	//Walking
	SuperWalkSpeed = BasicWalkSpeed + GetPawnAttributeValue("Athletics") * AthleticsScaleFactor;
	NormalWalkSpeed = BasicWalkSpeed;
	GetCharacterMovement()->MaxWalkSpeed = NormalWalkSpeed;

	//Pushing
	GetCharacterMovement()->PushForceFactor = GetPawnAttributeValue("Strength") * StrengthScaleFactor;

	//Physics
	GetCharacterMovement()->bRunPhysicsWithNoController = true;
	GetCharacterMovement()->AirControl = 1.0f;

	//Light
	LightComponent->SetVisibility(GetWorld()->GetFirstPlayerController() == this->GetController());

	//Cameras
	DetachedCamera->DetachFromParent(); //WTF: Why I need to do that?
	ActualCamera = ThirdPersonCamera;
	LastUsedCamera = FirstPersonCamera;
	ThirdPersonCamera->Activate();
	FirstPersonCamera->Deactivate();
	DetachedCamera->Deactivate();
	PlayerInfoComponent->AttachTo(ActualCamera);
	ObjectInfoComponent->SetCamera(ThirdPersonCamera);

	//VR
	bIsVREnabled = UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled();
	DetachedCamera->bUsePawnControlRotation = bIsVREnabled;
	ThirdPersonCamera->bUsePawnControlRotation = bIsVREnabled;
	CameraSpringArm->bEnableCameraRotationLag = bIsVREnabled;
	CameraSpringArm->CameraRotationLagSpeed = VRCameraRotationLag;
	CameraSpringArm->bEnableCameraLag = bIsVREnabled;
	CameraSpringArm->CameraLagSpeed = VRCameraMovementLag;
	bUseControllerRotationYaw = !bIsVREnabled;
	GetWorld()->GetFirstPlayerController()->PlayerCameraManager->bFollowHmdOrientation = bIsVREnabled;
}


/**************************** Tick *******************************************/

// Called every frame
void AZombieCharacter::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
	FVector HitLocation = TracingComponent->GetHitLocation();
	if (bIsPulling)
	{
		bool bIsCloseEnough = ((GetActorLocation() - HitLocation).Size() <= 200.0f) || ((GetActorLocation() - PullActor->GetActorLocation()).Size() <= 200.0f);
		if(bIsCloseEnough) {
			FVector PullActorOrigin, PullActorBounds;
			PullActor->GetActorBounds(true, PullActorOrigin, PullActorBounds);

			FVector ForwardVector = GetActorLocation() - PullActor->GetActorLocation();
			ForwardVector.Z = 0.0f;
			ForwardVector.Normalize();
			PullComponent->AddForceAtLocation(ForwardVector*GetCharacterMovement()->PushForceFactor, PullActorOrigin - PullActorBounds.Z / 2);
		}
	}
}


/**************************** Bind Input *************************************/

// Called to bind functionality to input
void AZombieCharacter::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);
	InputComponent->BindAction("ToggleRun", IE_Pressed, this, &AZombieCharacter::StartRunning);
	InputComponent->BindAction("ToggleRun", IE_Released, this, &AZombieCharacter::StopRunning);
	InputComponent->BindAction("PrimaryAction", IE_Pressed, this, &AZombieCharacter::PrimaryAction);
	InputComponent->BindAction("PrimaryAction", IE_Released, this, &AZombieCharacter::StopPulling);
	InputComponent->BindAction("SecondaryAction", IE_Pressed, this, &AZombieCharacter::SecondaryAction);
	InputComponent->BindAction("MoveUp", IE_Pressed, this, &AZombieCharacter::StartJump);
	InputComponent->BindAction("MoveUp", IE_Released, this, &AZombieCharacter::StopJump);
	InputComponent->BindAction("SwitchCamera", IE_Pressed, this, &AZombieCharacter::SwitchCamera);
	InputComponent->BindAction("DetachCamera", IE_Pressed, this, &AZombieCharacter::ToggleCameraDetachment);
	InputComponent->BindAction("MouseWheelUp", IE_Pressed, this, &AZombieCharacter::MoveCameraForward);
	InputComponent->BindAction("MouseWheelDown", IE_Pressed, this, &AZombieCharacter::MoveCameraBack);
	InputComponent->BindAction("ToggleTraceDotVisibility", IE_Pressed, this, &AZombieCharacter::ToggleTraceDotVisibility);
	InputComponent->BindAction("ResumeGame", IE_Pressed, this, &AZombieCharacter::ResumeGame);
	InputComponent->BindAction("ExitGame", IE_Pressed, this, &AZombieCharacter::ExitGame);
	InputComponent->BindAxis("MoveForward", this, &AZombieCharacter::ControlsMoveForward);
	InputComponent->BindAxis("MoveRight", this, &AZombieCharacter::ControlsMoveRight);
	InputComponent->BindAxis("TurnRight", this, &AZombieCharacter::ControlsTurnCameraRight);
	InputComponent->BindAxis("TurnUp", this, &AZombieCharacter::ControlsTurnCameraUp);
}

/**************************** Pull Object ************************************/

void AZombieCharacter::StartPulling()
{
	bIsPulling = false;
	PullActor = TracingComponent->GetTracedActor();
	PullComponent = TracingComponent->GetTracedComponent();
	if (PullComponent->IsSimulatingPhysics()) {
		bIsPulling = true;
		//FTimerHandle UnusedHandle;
		//GetWorld()->GetTimerManager().SetTimer(UnusedHandle, this, &AZombieCharacter::StopPulling, 0.5f, false);
	}
}

void AZombieCharacter::StopPulling()
{
	bIsPulling = false;
}

/**************************** Input Binded Move Character ********************/

void AZombieCharacter::ControlsMoveForward(float AxisValue)
{
	MoveForward(AxisValue);
}

void AZombieCharacter::ControlsMoveRight(float AxisValue)
{
	MoveRight(AxisValue);
}


/**************************** Input Binded Move Camera ***********************/

void AZombieCharacter::ControlsTurnCameraRight(float AxisValue)
{
	if (bIsVREnabled && ThirdPersonCamera->IsActive()) {
		AddActorWorldRotation(FRotator(0.0f, AxisValue, 0.0f));
		TurnCameraRight(AxisValue / 2.5);
	}
	else if (!DetachedCamera->IsActive())
		TurnRight(AxisValue);
	else
		TurnCameraRight(AxisValue);
}

void AZombieCharacter::ControlsTurnCameraUp(float AxisValue)
{
	if(!bIsVREnabled)
		TurnCameraUp(AxisValue);
}


/**************************** Moving Character *******************************/

//Move Character Forward
void AZombieCharacter::MoveForward(float AxisValue)
{
	FVector Direction;
	if (!ThirdPersonCamera->IsActive()) {
		Direction = GWorld->GetFirstPlayerController()->PlayerCameraManager->GetActorForwardVector();
		Direction.Z = 0.0f;
	}
	else
		Direction = GetActorForwardVector();
	Direction.Normalize();
	GetCharacterMovement()->AddInputVector(Direction * AxisValue);
}

//Move Character To Right
void AZombieCharacter::MoveRight(float AxisValue)
{
	FVector Direction;
	if (!ThirdPersonCamera->IsActive()) {
		Direction = GWorld->GetFirstPlayerController()->PlayerCameraManager->GetActorRightVector();
		Direction.Z = 0.0f;
	}
	else
		Direction = GetActorRightVector();
	Direction.Normalize();
	GetCharacterMovement()->AddInputVector(Direction * AxisValue);
}

//Turn Character Right
void AZombieCharacter::TurnRight(float AxisValue)
{
	GetWorld()->GetFirstPlayerController()->AddYawInput(AxisValue);
}

/**************************** Moving Camera **********************************/

//Look Up
void AZombieCharacter::TurnCameraUp(float AxisValue)
{
	//First Person Camera
	if (FirstPersonCamera->IsActive())
		GetWorld()->GetFirstPlayerController()->AddPitchInput(AxisValue);
	//Third Person Camera
	if (ThirdPersonCamera->IsActive()) {
		FRotator Rotator = FRotator(-AxisValue, 0, 0);
		CameraSpringArm->AddRelativeRotation(Rotator);
		float Pitch = CameraSpringArm->GetComponentRotation().Pitch;
		float PitchDif = FMath::Min(40.0f, Pitch);
		PitchDif = FMath::Max(-70.0f, PitchDif);
		CameraSpringArm->AddRelativeRotation(FRotator(PitchDif - Pitch, 0.0f, 0.0f));
	}
	//Detached Camera
	if (DetachedCamera->IsActive()) {
		DetachedCamera->AddRelativeRotation(FRotator(-AxisValue, 0.0f, 0.0f));
		float Pitch = DetachedCamera->GetComponentRotation().Pitch;
		float PitchDif = FMath::Min(80.0f, Pitch);
		PitchDif = FMath::Max(-80.0f, PitchDif);
		DetachedCamera->AddRelativeRotation(FRotator(PitchDif - Pitch, 0.0f, 0.0f));
	}
}

//Look Right
void AZombieCharacter::TurnCameraRight(float AxisValue)
{
	if(bIsVREnabled)
		GetWorld()->GetFirstPlayerController()->AddYawInput(AxisValue);
	else
		ActualCamera->AddRelativeRotation(FRotator(0.0f, AxisValue, 0.0f));
}

/**************************** Scrolling Third Person Camera ******************/

//Input Binded Scroll Forward
void AZombieCharacter::MoveCameraForward()
{
	MoveCamera(-1);
}

//Input Binded Scroll Back
void AZombieCharacter::MoveCameraBack()
{
	MoveCamera(1);
}

//Adjust Third Person Camera distance to character(scroll back and forward)
void AZombieCharacter::MoveCamera(int Direction)
{
	if (ThirdPersonCamera->IsActive()) {
		float relativeDest = Direction * CameraScrollSpeed + CameraSpringArm->TargetArmLength;
		if (relativeDest >= MinRelativeCameraOffset && relativeDest <= MaxRelativeCameraOffset)
			CameraSpringArm->TargetArmLength = relativeDest;
	}
}


/**************************** Jumping ****************************************/

void AZombieCharacter::StartJump()
{
	Jump();
}

void AZombieCharacter::StopJump()
{
	StopJumping();
}

/**************************** Running ****************************************/

void AZombieCharacter::StartRunning()
{
	bIsRunning = true;
	GetCharacterMovement()->MaxWalkSpeed = SuperWalkSpeed;
	GetCharacterMovement()->JumpZVelocity = SuperJumpVelocity;
}

void AZombieCharacter::StopRunning()
{
	bIsRunning = false;
	GetCharacterMovement()->MaxWalkSpeed = NormalWalkSpeed;
	GetCharacterMovement()->JumpZVelocity = NormalJumpVelocity;
}

/**************************** Actions ****************************************/

//Use objects
void AZombieCharacter::PrimaryAction()
{
	TracingComponent->HandleInteractEvent();
	StartPulling();
}

//Abandon body - change to ghost
void AZombieCharacter::SecondaryAction()
{
	if (GetMovementComponent()->IsActive())
	{
		PrepareToAbandonBody();
		if (UsedCameraName == "DetachedCamera")
			UsedCameraName = "ThirdPersonCamera";
		AGhostCharacter* GhostPawn = GetWorld()->SpawnActor<AGhostCharacter>(AGhostCharacter::StaticClass());
		GhostPawn->PrepareToBePossessed();
		GhostPawn->SetActorLocationAndRotation(GetActorLocation(), GetActorRotation());
		if(bIsRunning)
			GhostPawn->IncreaseSpeed();
		GetWorld()->GetFirstPlayerController()->Possess(GhostPawn);
	}
}


/**************************** Camera Switching *******************************/

//Input Binded - Switch between cameras
void AZombieCharacter::SwitchCamera()
{
	if (FirstPersonCamera->IsActive())
		SwitchToCamera(ThirdPersonCamera);
	else if (ThirdPersonCamera->IsActive())
		SwitchToCamera(FirstPersonCamera);
	else if (DetachedCamera->IsActive())
		SwitchToCamera(LastUsedCamera);
}

//Input Binded - Detach/attach camera from character
void AZombieCharacter::ToggleCameraDetachment()
{
	if (DetachedCamera->IsActive())
		SwitchToCamera(LastUsedCamera);
	else {
		DetachedCamera->SetWorldLocation(ActualCamera->GetComponentLocation());
		DetachedCamera->SetWorldRotation(ActualCamera->GetComponentRotation());
		SwitchToCamera(DetachedCamera);
	}
}

//Switch to specified Camera
void AZombieCharacter::SwitchToCamera(UCameraComponent* TargetCamera)
{
	if (ActualCamera == TargetCamera)
		return;
	if (DetachedCamera->IsActive()) {
		GetCharacterMovement()->bOrientRotationToMovement = false;
		bUseControllerRotationYaw = true;
	}
	if(ThirdPersonCamera->IsActive() && bIsVREnabled)
		bUseControllerRotationYaw = true;
	TargetCamera->Activate();
	ActualCamera->Deactivate();

	LastUsedCamera = ActualCamera;
	ActualCamera = TargetCamera;

	PlayerInfoComponent->AttachTo(TargetCamera);
	ObjectInfoComponent->SetCamera(TargetCamera);

	if (DetachedCamera->IsActive()) {
		GetCharacterMovement()->bOrientRotationToMovement = true;
		bUseControllerRotationYaw = false;
	}
	if (ThirdPersonCamera->IsActive() && bIsVREnabled) {
		CameraSpringArm->bEnableCameraRotationLag = false;
		CameraSpringArm->bEnableCameraLag = false;
		UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
		FTimerHandle UnusedHandle;
		GetWorld()->GetTimerManager().SetTimer(UnusedHandle, this, &AZombieCharacter::UpdateAfterPossession, 0.01f, false);
	}
}

/**************************** Trace Dot    ***********************************/
//Input Binded
void AZombieCharacter::ToggleTraceDotVisibility()
{
	TracingComponent->SetTraceDotVisibility(!TracingComponent->IsTraceDotVisible());
}

/**************************** Resume Game  ***********************************/
void AZombieCharacter::ResumeGame()
{
	if (bWaitForInput && !GetMovementComponent()->IsActive())
	{
		PlayerInfoComponent->Hide();
		GetMesh()->bPauseAnims = false;
		GetMovementComponent()->Activate();
	}
}

void AZombieCharacter::WaitForInput()
{
	bWaitForInput = true;
}

void AZombieCharacter::ExitGame()
{
	GetWorld()->GetFirstPlayerController()->ConsoleCommand("quit");
}

/**************************** IDisplayable ***********************************/

FString AZombieCharacter::GetNameToDisplay()
{
	return DisplayableName;
}

TArray<FPawnAttribute> AZombieCharacter::GetAttributes()
{
	return PawnAttributes;
}

FVector AZombieCharacter::GetDisplayLocation()
{
	FVector Origin, TracedActorBounds;
	Cast<AActor>(this)->GetActorBounds(true, Origin, TracedActorBounds);

	return Cast<AActor>(this)->GetActorLocation() + FVector(0.0f, 0.0f, TracedActorBounds.Z - 25.0f);
}

/**************************** IInteractive ***********************************/

bool AZombieCharacter::IsInteractEnabled(AActor* actor, FInteractCapability* output)
{
	return true;
}

//Is being possessed
void AZombieCharacter::Interact(AActor* actor)
{
	TracingComponent->ActivateTracer();
	LightComponent->SetVisibility(true);

	GetWorld()->GetFirstPlayerController()->Possess(this);

	if (UsedCameraName == "DetachedCamera") {
		DetachedCamera->SetWorldLocation(DetachedCameraLocation);
		DetachedCamera->SetWorldRotation(DetachedCameraRotation);
		SwitchToCamera(DetachedCamera);
	}
	else if (UsedCameraName == "FirstPersonCamera")
		SwitchToCamera(FirstPersonCamera);
	else if (UsedCameraName == "ThirdPersonCamera") {
		SwitchToCamera(ThirdPersonCamera);
		if (bIsVREnabled)
			UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
	}
}

/****************************  IPawnInterface ********************************/

// Player Info Component Related

void AZombieCharacter::DisplayInfo(FString TextInfo)
{
	bWaitForInput = false;
	FTimerHandle UnusedHandle;
	GetWorld()->GetTimerManager().SetTimer(UnusedHandle, this, &AZombieCharacter::WaitForInput, 0.5f, false);
	GetMovementComponent()->Deactivate();
	GetMesh()->bPauseAnims = true;
	PlayerInfoComponent->Show(TextInfo);
}

// Object Info Component Related

void AZombieCharacter::DisplayObjectInfo(IDisplayable* TracedActor, FInteractCapability* InteractCapability)
{
	ObjectInfoComponent->Display(TracedActor, InteractCapability);
}
void AZombieCharacter::HideObjectInfo()
{
	ObjectInfoComponent->Hide();
}

// Interact Related

FString AZombieCharacter::GetInteractiveName()
{
	return "Zombie";
}

void AZombieCharacter::InteractWith(AActor* Actor)
{
	//If we are abandoning this body, we need to prepare for that
	AZombieCharacter* Pawn = Cast<AZombieCharacter>(Actor);
	if (Pawn != nullptr) {
		PrepareToAbandonBody();
		if (bIsRunning)
			Pawn->StartRunning();
		else
			Pawn->StopRunning();
	}
}

// Attributes Related

int AZombieCharacter::GetPawnAttributeValue(FString AttributeName)
{
	for (int i = 0; i < PawnAttributes.Num(); i++)
		if (PawnAttributes[i].Name == AttributeName)
			return PawnAttributes[i].Value;
	return 0;
}

/**************************** Possession / Abandoning body *******************/

void AZombieCharacter::PrepareToAbandonBody()
{
	LightComponent->SetVisibility(false);
	TracingComponent->DeactivateTracer();
	DetachedCameraLocation = DetachedCamera->GetComponentLocation();
	DetachedCameraRotation = DetachedCamera->GetComponentRotation();
	UsedCameraName = ActualCamera->GetName();
}

void AZombieCharacter::UpdateAfterPossession()
{
	CameraSpringArm->bEnableCameraRotationLag = true;
	CameraSpringArm->bEnableCameraLag = true;
	//bUseControllerRotationYaw = false;
}


FString AZombieCharacter::UsedCameraName = "ThirdPersonCamera";
FVector AZombieCharacter::DetachedCameraLocation = FVector(0.0f, 0.0f, 0.0f);
FRotator AZombieCharacter::DetachedCameraRotation = FRotator(0.0f, 0.0f, 0.0f);