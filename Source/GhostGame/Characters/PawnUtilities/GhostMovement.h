// Custom movement component for a ghost

#pragma once

#include "GameFramework/PawnMovementComponent.h"
#include "GhostMovement.generated.h"

UCLASS()
class GHOSTGAME_API UGhostMovement : public UPawnMovementComponent
{
	GENERATED_BODY()

private:
	const int SKIP_RATE = 50;
	const int LOW_SPEED = 400;
	const int HIGH_SPEED = 1000;
	int skip = 0;
	int speed = LOW_SPEED;
	bool vrmode = false;
public:
	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction) override;
	void ToggleVRMode();
	void SetSpeedBoost(bool boost);
	bool IsFast();
};