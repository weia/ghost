// Author: Grzegorz Bukowiec

#include "GhostGame.h"
#include "GhostMovement.h"


void UGhostMovement::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction)
{
	if (skip && vrmode)
	{
		--skip;
	}
	else
	{
		skip = SKIP_RATE;

		Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
		// Make sure that everything is still valid, and that we are allowed to move.
		if (!PawnOwner || !UpdatedComponent || ShouldSkipUpdate(DeltaTime))
		{
			return;
		}

		// Get (and then clear) the movement vector
		FVector DesiredMovementThisFrame = ConsumeInputVector().GetClampedToMaxSize(1.0f) * DeltaTime * speed * (vrmode? SKIP_RATE : 1);
		if (!DesiredMovementThisFrame.IsNearlyZero())
		{
			FHitResult Hit;
			SafeMoveUpdatedComponent(DesiredMovementThisFrame, UpdatedComponent->GetComponentRotation(), true, Hit);
			// Slide along if you hit something
			if (Hit.IsValidBlockingHit())
			{
				SlideAlongSurface(DesiredMovementThisFrame, 1.f - Hit.Time, Hit.Normal, Hit);
			}
		}
	}
};

void UGhostMovement::ToggleVRMode()
{
	vrmode = !vrmode;
}

void UGhostMovement::SetSpeedBoost(bool boost)
{
	speed = boost ? HIGH_SPEED : LOW_SPEED;
}

bool UGhostMovement::IsFast()
{
	return speed == HIGH_SPEED;
}