// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GhostGame.h"
#include "GameFramework/Character.h"
#include "../Components/ObjectInfoComponent.h"
#include "../Components/TracingComponent.h"
#include "../Interfaces/Interactive.h"
#include "../Interfaces/Displayable.h"
#include "../Interfaces/PawnInterface.h"
#include "../Utilities/PawnAttribute.h"
#include "../Utilities/InteractCapability.h"
#include "ZombieCharacter.generated.h"

UCLASS()
class GHOSTGAME_API AZombieCharacter : public ACharacter, public IPawnInterface, public IInteractive, public IDisplayable
{
	GENERATED_BODY()
private:
	//Components
	UCameraComponent* FirstPersonCamera;
	UCameraComponent* ThirdPersonCamera;
	UCameraComponent* DetachedCamera; 
	USpringArmComponent* CameraSpringArm;
	UTracingComponent* TracingComponent;
	UPointLightComponent* LightComponent;
	UPlayerInfoComponent* PlayerInfoComponent;
	UObjectInfoComponent* ObjectInfoComponent;

	//Camera related variables
	const int InitialThirdPersonCameraOffset = 350;
	const int CameraScrollSpeed = 20;
	const int MinRelativeCameraOffset = 100;
	const int MaxRelativeCameraOffset = 500;
	const float VRCameraRotationLag = 3.0f;
	const float VRCameraMovementLag = 2.0f;
	bool bIsVREnabled;
	UCameraComponent* ActualCamera;
	UCameraComponent* LastUsedCamera;

	//PlayerInfoComponent related variables
	bool bWaitForInput = true;

	//Pulling Related Variables
	AActor* PullActor;
	UPrimitiveComponent* PullComponent;
	bool bIsPulling = false;

	//To remember which camera we were using
	static FString UsedCameraName;
	static FVector DetachedCameraLocation;
	static FRotator DetachedCameraRotation;


	//Attribute related variables
	int NormalJumpVelocity;
	int SuperJumpVelocity;
	int NormalWalkSpeed;
	int SuperWalkSpeed;
	const int BasicWalkSpeed = 500;
	const int BasicJumpVelocity = 600;
	const int AcrobaticsScaleFactor = 20;
	const int AthleticsScaleFactor = 20;
	const int StrengthScaleFactor = 2000;

	bool bIsRunning = false;

	//Functions
	void SwitchToCamera(UCameraComponent* TargetCamera);
	void MoveForward(float AxisValue);
	void MoveRight(float AxisValue);
	void TurnRight(float AxisValue);
	void TurnCameraRight(float AxisValue);
	void TurnCameraUp(float AxisValue);
	void PrepareToAbandonBody();
	void UpdateAfterPossession();
	void ResumeGame();
	void WaitForInput();
	void ExitGame();

	void StartPulling();
	void StopPulling();

public:
	//Array of our character's attributes(Strength, Speed, and so on)
	UPROPERTY(EditAnywhere)
		TArray<FPawnAttribute> PawnAttributes;

	//Displayable name for this character
	UPROPERTY(EditAnywhere)
		FString DisplayableName = "Possess body";

	AZombieCharacter();
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void Tick( float DeltaSeconds ) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
	
	// Functions binded to input
	void ControlsMoveForward(float AxisValue);
	void ControlsMoveRight(float AxisValue);
	void ControlsTurnCameraRight(float AxisValue);
	void ControlsTurnCameraUp(float AxisValue);
	void StartJump();
	void StopJump();
	void StartRunning();
	void StopRunning();
	void PrimaryAction();
	void SecondaryAction();
	void SwitchCamera();
	void ToggleCameraDetachment();
	void MoveCamera(int Direction);
	void MoveCameraForward();
	void MoveCameraBack();
	void ToggleTraceDotVisibility();

	//Inherited from IDisplayable
	FString GetNameToDisplay() override;
	TArray<FPawnAttribute> GetAttributes() override;
	FVector GetDisplayLocation() override;

	//Inherited from IInteractive
	void Interact(AActor* actor) override;
	bool IsInteractEnabled(AActor* actor, FInteractCapability* output) override;

	//Inherited from IPawnInterface
	void DisplayInfo(FString TextInfo) override;
	void DisplayObjectInfo(IDisplayable* TracedActor, FInteractCapability* InteractCapability) override;
	void HideObjectInfo() override;
	void InteractWith(AActor* actor);
	FString GetInteractiveName() override;
	int GetPawnAttributeValue(FString AttrubuteName) override;
};

